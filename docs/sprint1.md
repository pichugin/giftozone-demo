

Accounts:
---------
- Registration Form: email, pwd, name.
- Confirmation email.
- Login Form: email, pwd.
- Reset pwd.
- Session validation prior to any AJAX call - let the user to login on the fly.

Events (Target):
----------------
- Create event
- Invite people to an event.
- Add items already owned by the person;
- Add your proposals;
- Comment on proposals;
- Request to join somebody else's proposal;

