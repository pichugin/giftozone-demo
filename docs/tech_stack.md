
Client:
-------
- SPA
- Angular?
- Login page detection in Ajax callbacks;
- JS resources versioning;

Server:
-------
- Spring MVC
- Spring Security
- Spring Data
- MongoDB

Deployment:
-----------
- Jelastic
- Tomcat / Jetty

