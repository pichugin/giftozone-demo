#
# OccasionController
#
GET		/api/occasions       - get the list of occasions
POST		/api/occasions       - add occasion
POST		/api/occasions/{id}  - modify occasion
DELETE	/api/occasions/{id}  - delete occasion

POST		/api/occasions/{id}/comments       - add ...
POST		/api/occasions/{id}/comments/{id}  - modify ...
DELETE	/api/occasions/{id}/comments/{id}  - delete ...

POST		/api/occasions/{id}/participants/{id}  - add (invite)...
DELETE	/api/occasions/{id}/participants/{id}  - delete ...

#
# GiftBagController
#
GET		/api/occasions/{id}/giftBags       - get the list of gifts
POST		/api/occasions/{id}/giftBags       - add gift
POST		/api/occasions/{id}/giftBags/{id}  - modify gift
DELETE	/api/occasions/{id}/giftBags/{id}  - delete gift

POST		/api/occasions/{id}/giftBags/{id}/participants/{id}  - add (invite)...
DELETE	/api/occasions/{id}/giftBags/{id}/participants/{id}  - delete ...

POST		/api/occasions/{id}/giftBags/{id}/comments       - add (invite)...
POST		/api/occasions/{id}/giftBags/{id}/comments/{id}  - modify ...
DELETE	/api/occasions/{id}/giftBags/{id}/comments/{id}  - delete ...

GET		/api/occasions/{id}/giftBags/{id}/giftProposals       - get the list of ...
POST		/api/occasions/{id}/giftBags/{id}/giftProposals       - add ...
POST		/api/occasions/{id}/giftBags/{id}/giftProposals/{id}  - modify ...
DELETE	/api/occasions/{id}/giftBags/{id}/giftProposals/{id}  - delete ...

POST		/api/occasions/{id}/giftBags/{id}/giftProposals/{id}/comments       - add ...
POST		/api/occasions/{id}/giftBags/{id}/giftProposals/{id}/comments/{id}  - modify ...
DELETE	/api/occasions/{id}/giftBags/{id}/giftProposals/{id}/comments/{id}  - delete ...

POST		/api/occasions/{id}/giftBags/{id}/giftProposals/{id}/voters       - vote
DELETE	/api/occasions/{id}/giftBags/{id}/giftProposals/{id}/voters/{id}  - unvote

#
# InvitationController
#
GET		/api/invitations  - get user's invitations
POST		/api/invitations  - invite a user to occasion/giftBag
POST		/api/invitations/{invitationId} - accept invitation to GiftBag
GET		/acceptinvitation/{invitationId} - accept invitation to Occasion  (unsecured URL)
