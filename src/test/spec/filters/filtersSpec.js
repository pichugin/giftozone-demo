describe('filters', function() {

	beforeEach(function() {
		module('giftozone.filters');  
	});

	describe('gzUserIdToName', function() {
		it('should convert user ids to user names', inject(function(gzUserIdToNameFilter) {
			var userContacts = [{"userId":"user1","displayName":"Bob"},{"userId":"user2","displayName":"Jake"},{"userId":"user3"}];
			expect(gzUserIdToNameFilter("user1", undefined)).toBe("user1");
			expect(gzUserIdToNameFilter("user1", [])).toBe("user1");
			expect(gzUserIdToNameFilter("user1", userContacts)).toBe("Bob");
			expect(gzUserIdToNameFilter("user2", userContacts)).toBe("Jake");
			expect(gzUserIdToNameFilter("user3", userContacts)).toBe("user3");
			expect(gzUserIdToNameFilter("user4", userContacts)).toBe("user4");
			expect(gzUserIdToNameFilter(undefined, userContacts)).toBe("");
			expect(gzUserIdToNameFilter(undefined, [])).toBe("");
			expect(gzUserIdToNameFilter(undefined, undefined)).toBe("");
			expect(gzUserIdToNameFilter(undefined, [])).toBe("");
		}));
	});
	
	describe('gzUserIdToEmail', function() {
		it('should convert user ids to user emails', inject(function(gzUserIdToEmailFilter) {
			var userContacts = [{"userId":"user1","email":"bob.robertson@gmail.com"},{"userId":"user2","email":"r_bobbertson@live.ca"},{"userId":"user3"}];
			expect(gzUserIdToEmailFilter("user1", undefined)).toBe("");
			expect(gzUserIdToEmailFilter("user1", [])).toBe("");
			expect(gzUserIdToEmailFilter("user1", userContacts)).toBe("bob.robertson@gmail.com");
			expect(gzUserIdToEmailFilter("user2", userContacts)).toBe("r_bobbertson@live.ca");
			expect(gzUserIdToEmailFilter("user3", userContacts)).toBe("");
			expect(gzUserIdToEmailFilter("user4", userContacts)).toBe("");
			expect(gzUserIdToEmailFilter(undefined, userContacts)).toBe("");
			expect(gzUserIdToEmailFilter(undefined, [])).toBe("");
			expect(gzUserIdToEmailFilter(undefined, undefined)).toBe("");
			expect(gzUserIdToEmailFilter(undefined, [])).toBe("");
		}));
	})

	describe('gzConcatUsers', function() {
		it('should concatenate users by id', inject(function(gzConcatUsersFilter) {
			var userContacts = [{"userId":"user1","displayName":"James"}];
			expect(gzConcatUsersFilter([], userContacts)).toBe("");
			expect(gzConcatUsersFilter(["user1"], userContacts)).toBe("James");
			expect(gzConcatUsersFilter(["user1", "user2"], userContacts)).toBe("James, user2");
		}));
	});
	
	describe('gzConcatGiftProposals', function() {
		it('should concatenate gift proposal names', inject(function(gzConcatGiftProposalsFilter) {
			expect(gzConcatGiftProposalsFilter([])).toBe("");
			expect(gzConcatGiftProposalsFilter(undefined)).toBe("");
			expect(gzConcatGiftProposalsFilter([])).toBe("");
			expect(gzConcatGiftProposalsFilter([{name : "proposal1"}])).toBe("proposal1");
			expect(gzConcatGiftProposalsFilter([{name : "proposal1"}, {name : "proposal2"}])).toBe("proposal1, proposal2");
		}));
	});
	
	describe('gzDate', function() {
		it('should convert date to human readable format', inject(function(gzDateFilter) {
			expect(gzDateFilter("2014-12-31")).toBe("Dec 31, 2014");
			expect(gzDateFilter("1970-01-01")).toBe("Jan 1, 1970");
			expect(gzDateFilter(new Date(900000000000))).toBe("Jul 9, 1998");
			expect(gzDateFilter("")).toBe("");
			expect(gzDateFilter(undefined)).toBe("");
		}));
	});
	
	describe('gzFromNow', function() {
		it('should convert date to human readable format in a time from now', inject(function(gzFromNowFilter) {
			var msInHr = 3600000;
			var now = new Date();
			var yesterday = new Date(now.getTime() - (msInHr * 24))
			var inOneHr = new Date(now.getTime() + msInHr);
			var tomorrow = new Date(now.getTime() + (msInHr * 24));
			expect(gzFromNowFilter(yesterday)).toBe("a day ago");
			expect(gzFromNowFilter(now)).toBe("a few seconds ago");
			expect(gzFromNowFilter(inOneHr)).toBe("in an hour");
			expect(gzFromNowFilter(tomorrow)).toBe("in a day");
			expect(gzFromNowFilter("")).toBe("");
			expect(gzFromNowFilter(undefined)).toBe("");
		}));
	});
	
	describe('gzNumToDollars', function() {
		it('should convert number to a dollar string', inject(function(gzNumToDollarsFilter) {
			expect(gzNumToDollarsFilter(undefined)).toBe("$0.00");
			expect(gzNumToDollarsFilter(0)).toBe("$0.00");
			expect(gzNumToDollarsFilter(1)).toBe("$1.00");
			expect(gzNumToDollarsFilter(12.3)).toBe("$12.30");
			expect(gzNumToDollarsFilter(12.34)).toBe("$12.34");
			expect(gzNumToDollarsFilter(-5)).toBe("-$5.00");
		}));
	});

});