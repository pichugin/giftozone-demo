describe('UserProfileCtrl', function() {
	var $rootScope, $scope, ctrl;
	
	beforeEach(function() {
		module('giftozone.controllers');
		
		inject(function($injector) {
			$rootScope = $injector.get('$rootScope');
			$scope = $rootScope.$new();
			
			var userServiceMock = {
					getCurrentUser : function(cb) {
						cb({ userId : "user1" });
					},
					getUser : function(userId, cb) {
						cb({ userId : "user1" });
					},
					updateUser : function(userAccount, cb) {
						cb(userAccount);
					}
				};
			
			ctrl = $injector.get('$controller')("UserProfileCtrl", {
					$scope : $scope,
					$routeParams : {}, 
					UserService : userServiceMock
				});
		});
	});

	it('should initialize correctly', function() {
		expect($scope.currentUser.userId).toEqual("user1");
		expect($scope.userAccount.userId).toEqual("user1");
		expect($scope.userAccount.displayName).toBeUndefined();
		expect($scope.userContacts).toEqual([]);
		expect($scope.pageState).toEqual({});
	});
	
	it('should let you edit the user', function() {
		$scope.editUserProfile();
		expect($scope.pageState.editUserProfile).toBe(true);
		$scope.userAccount.displayName = "James Smith";
		expect($scope.isUserUnchanged()).toBe(false);
		$scope.saveEditUserProfile();
		expect($scope.userAccount.displayName).toEqual("James Smith");
	});
	
	it('should let you cancel the edit', function() {
		$scope.editUserProfile();
		$scope.userAccount.displayName = "James Smith";
		$scope.cancelEditUserProfile();
		expect($scope.userAccount.displayName).toBeUndefined();
	});
});