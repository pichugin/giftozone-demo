//http://nathanleclaire.com/blog/2013/12/13/how-to-unit-test-controllers-in-angularjs-without-setting-your-hair-on-fire/
describe('ConfirmModalCtrl', function() {

	var $scope, $location, $rootScope, modalInstance, createController;

	beforeEach(function() {
		module('giftozone.controllers');  
	});

    beforeEach(inject(function($injector) {
        $location = $injector.get('$location');
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();

        var $controller = $injector.get('$controller');

        modalInstance = {
				close : function() {},
				dismiss : function() {}
        	};
        	
        createController = function() {
            return $controller('ConfirmModalCtrl', {
                '$scope' : $scope,
                '$modalInstance' : modalInstance,
                'message' : 'test message'
            });
        };
        
        spyOn(modalInstance, 'close');
        spyOn(modalInstance, 'dismiss');
    }));

	it('should initialize correctly', function() {
		var controller = createController();
		expect($scope.message).toBe("test message");
		expect($scope.ok).toBeDefined();
		expect($scope.cancel).toBeDefined();
	});
	
	it('makes sure close was called', function() {
		var controller = createController();
		$scope.ok();
		expect(modalInstance.close).toHaveBeenCalled();
		expect(modalInstance.dismiss).not.toHaveBeenCalled();
	});
	
	it('makes sure dismiss was called', function() {
		var controller = createController();
		$scope.cancel();
		expect(modalInstance.dismiss).toHaveBeenCalled();
		expect(modalInstance.close).not.toHaveBeenCalled();
	});

});