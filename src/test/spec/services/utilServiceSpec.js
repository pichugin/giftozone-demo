describe('services', function() {

	var service;

	beforeEach(function($provide) {
		module('giftozone.services');
		
		inject(function($injector) {
			service = $injector.get("Utils");
		});
	});

	describe('Utils', function() {
		it('should be able to get index by id', function() {
			expect(service.getIndexById(null, null)).toEqual(-1);
			expect(service.getIndexById([], null)).toEqual(-1);
			expect(service.getIndexById([{id:1}, {id:2}, {id:3}], 1)).toEqual(0);
			expect(service.getIndexById([{id:1}, {id:2}, {id:3}], 3)).toEqual(2);
			expect(service.getIndexById([{id:1}, {id:2}, {id:3}], 4)).toEqual(-1);
			expect(service.getIndexById([{id:'1'}, {id:'2'}, {id:'3'}], '2')).toEqual(1);
		});
		
		it('should convert dates to strings', function() {
			expect(service.dateToString_MMDDYYYY(new Date('2014', '11', '25'))).toEqual("12-25-2014");
			expect(service.dateToString_MMDDYYYY(new Date('2014', '0', '05'))).toEqual("01-05-2014");
		});
	});

});