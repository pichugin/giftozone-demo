describe('services', function() {
	
	var mock;
	
	beforeEach(function($provide) {
		module('giftozone.services');

		mock = {
			alert : jasmine.createSpy()
		};

		module(function($provide) {
			$provide.value('$window', mock);
		});
	});

	describe('ErrorHandler', function() {
		it('should call alert with no message or status', inject(function(ErrorHandler) {
			ErrorHandler.error();
			expect(mock.alert).toHaveBeenCalledWith("Error!");
		}));
		
		it('should call alert with a status', inject(function(ErrorHandler) {
			ErrorHandler.error('data', 'status', 'headers', 'config');
			expect(mock.alert).toHaveBeenCalledWith("Error! (status)");
		}));
	});

});