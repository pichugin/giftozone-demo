<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="include/head.jsp"></jsp:include>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="./">GiftOZone</a>
			</div>
		</div>
	</div>
	<div class="row header-row">
		<div class="container">
			<div class="row">
				<div class="col-md-12" id="headerContainer">
					<header>
						<h1>Sign Up</h1>
					</header>
				</div>
			</div>
		</div>
	</div>
	<div class="container sign-up">
		<div class="row">
			<div class="col-md-6">
				<section id="signUpSection">
					<h2>Create New Account</h2>
					<form:form action="signup" method="POST" class="form-horizontal" role="form" modelAttribute="signupForm">
						<div class="form-group">
							<label class="control-label col-md-2" for="inputUsername">Name:</label>
							<div class="controls col-md-6">
 								<form:input path="username" id="inputUsername" placeholder="Name" cssClass="form-control" autofocus="true" />
								<form:errors path="username" />
							</div>
						</div>
					
						<div class="form-group">
							<label class="control-label col-md-2" for="inputEmail">Email:</label>
							<div class="controls col-md-6">
								<form:input type="email" path="email" id="inputEmail" placeholder="Email" cssClass="form-control" />
								<form:errors path="email" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2" for="inputPassword">Password:</label>
							<div class="controls col-md-6">
								<input type="password" name="password" id="inputPassword" placeholder="Password" class="form-control" />
								<form:errors path="password" />
							</div>
						</div>
						<div class="form-group">
							<div class="controls col-md-offset-2 col-md-6">
								<input type="submit" value="Sign Up" class="btn btn-primary" />
							</div>
						</div>
					</form:form>
				</section>
			</div>
			<div class="col-md-6">
				<section id="socialSignInSection">
					<h2>Use Existing Account</h2>
					<div class="col-md-6 col-md-offset-1 sign-up-existing-well">
						<form action="signin/facebook" method="POST">
							<div class="form-group">
								<button type="submit" class="btn btn-block"><img src="img/fb_logo.png" alt="Facebook logo" /> Sign Up with Facebook</button>
							</div>
						</form>
						
						<form action="signin/google" method="POST">
							<div class="form-group">
								<input type="hidden" name="_spring_security_remember_me" value="true" />
								<button type="submit" class="btn btn-block"><img src="img/google_logo.png" alt="Google logo" /> Sign Up with Google</button>
							</div>
						</form>
					
						<form action="signin/twitter" method="POST">
							<div class="form-group">
								<button type="submit" class="btn btn-block"><img src="img/twitter_logo.png" alt="Facebook logo" /> Sign Up with Twitter</button>
							</div>
						</form>
					</div>
				</section>
			</div>
		</div>
	</div>
</body>
</html>