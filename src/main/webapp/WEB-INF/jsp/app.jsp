<!DOCTYPE html>
<html ng-app="giftozone">
<head>
	<jsp:include page="include/head.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="include/appNavbar.jsp"></jsp:include>
	
	<div ng-view></div>
	
	<footer>
		<p>� 2013 AppYaks</p>
	</footer>
	
	<jsp:include page="include/scripts.jsp"></jsp:include>
</body>
</html>