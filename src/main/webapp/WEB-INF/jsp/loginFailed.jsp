<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="include/head.jsp"></jsp:include>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="./">GiftOZone</a>
			</div>
		</div>
	</div>
	<div class="row header-row">
		<div class="container">
			<div class="row">
				<div class="col-md-12" id="headerContainer">
					<header>
						<h1>Login</h1>
					</header>
				</div>
			</div>
		</div>
	</div>
	<div class="container sign-up">
		<div class="row">
			<div class="col-md-12">
				<section id="signUpSection">
					<h2>
						Login failed: 
						<c:if test="${SPRING_SECURITY_LAST_EXCEPTION != null}">
							<c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"/>
						</c:if>
					</h2>
					<span><a href="#">Reset password (TODO)</a></span>
				</section>
			</div>
		</div>
	</div>
</body>
</html>