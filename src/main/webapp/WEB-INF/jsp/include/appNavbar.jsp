<div class="navbar navbar-fixed-top navbar-inverse" role="navigation" ng-controller="NavbarCtrl">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#/occasions">GiftOZone</a>
		</div>
		<div class="collapse navbar-collapse">
			<!-- 
			<ul class="nav navbar-nav">
				<li><a href="#/occasions">Occasions</a></li>
				<li>
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Gift Bags <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li class="dropdown-header">Recently Viewed</li>
						<li ng-repeat="giftBag in recentGiftBags">
							<a href="#/occasion/{{giftBag.occasionId}}/giftbag/{{giftBag.id}}">Gift Bag</a>
						</li>
					</ul>
				</li>
			</ul>
			-->
			
			<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-question-sign"></span> <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#/about">About</a></li>
						<li><a href="#" ng-click="openFeedbackFormModal()">Feedback</a></li>
					</ul>
				</li>
				<li>
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> <span ng-bind="userAccount.displayName || userAccount.email"></span> <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#/user/{{userAccount.userId}}"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
						<li>
							<a href="javascript:$('#connectWithFacebookForm').submit()"><img src="img/fb_logo_sm.png" alt="Facebook logo" /> Connect with Facebook</a>
							<form id="connectWithFacebookForm" action="connect/facebook" method="POST"></form>
						</li>
						<li>
							<a href="javascript:$('#connectWithGoogleForm').submit()"><img src="img/google_logo_sm.png" alt="Google logo" /> Connect with Google</a>
							<form id="connectWithGoogleForm" action="connect/google" method="POST">
								<input type="hidden" name="scope" value="https://www.googleapis.com/auth/userinfo.profile" />
							</form>
						</li>
						<li>
							<a href="javascript:$('#connectWithTwitterForm').submit()"><img src="img/twitter_logo_sm.png" alt="Twitter logo" /> Connect with Twitter</a>
							<form id="connectWithTwitterForm" action="connect/twitter" method="POST"></form>
						</li>
						<li class="divider"></li>
						<li><a href="signout">Sign out</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>