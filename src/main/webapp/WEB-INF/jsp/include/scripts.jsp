<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.4/angular.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.4/angular-route.js"></script>

<script type="text/javascript" src="js/ui-bootstrap-tpls-0.9.0.min.js"></script>
<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/jquery.calendario.js"></script>
<script type="text/javascript" src="js/util.js"></script>

<script type="text/javascript" src="ng/app.js"></script>

<script type="text/javascript" src="ng/controllers/aboutController.js"></script>
<script type="text/javascript" src="ng/controllers/commentController.js"></script>
<script type="text/javascript" src="ng/controllers/confirmModalController.js"></script>
<script type="text/javascript" src="ng/controllers/feedbackModalController.js"></script>
<script type="text/javascript" src="ng/controllers/giftBagController.js"></script>
<script type="text/javascript" src="ng/controllers/navbarController.js"></script>
<script type="text/javascript" src="ng/controllers/occasionController.js"></script>
<script type="text/javascript" src="ng/controllers/userProfileController.js"></script>

<script type="text/javascript" src="ng/directives/directives.js"></script>

<script type="text/javascript" src="ng/filters/filters.js"></script>

<script type="text/javascript" src="ng/services/commentService.js"></script>
<script type="text/javascript" src="ng/services/errorHandlerService.js"></script>
<script type="text/javascript" src="ng/services/feedbackService.js"></script>
<script type="text/javascript" src="ng/services/giftBagService.js"></script>
<script type="text/javascript" src="ng/services/invitationService.js"></script>
<script type="text/javascript" src="ng/services/occasionService.js"></script>
<script type="text/javascript" src="ng/services/userService.js"></script>
<script type="text/javascript" src="ng/services/utilService.js"></script>