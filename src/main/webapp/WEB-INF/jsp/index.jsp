<!DOCTYPE html>
<html>
<head>
	<jsp:include page="include/head.jsp"></jsp:include>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="./">GiftOZone</a>
			</div>
			<div class="navbar-collapse collapse" style="height: 1px;">
				<form class="navbar-form navbar-right" role="form" action="j_spring_security_check" method="POST">
					<div class="form-group">
						<input type="email" name="j_username" class="form-control" placeholder="Email" value="" autofocus />
					</div>
					<div class="form-group">
						<input type="password" name="j_password" class="form-control" placeholder="Password" value="" />
					</div>
					<button type="submit" class="btn btn-success">Sign in</button>
				</form>
			</div>
			<!--/.navbar-collapse -->
		</div>
	</div>
	
	<div class="container">
		<div class="jumbotron">
			<h1>Welcome to GiftOZone!</h1>
			<p>GiftOZone is an epic new app that makes gift giving a breeze.</p>
			<p><a href="signup" class="btn btn-warning btn-large">Sign Up �</a></p>
		</div>
		<div class="row login-marketing">
			<div class="col-md-4">
				<h2>Never</h2>
				<p>Get the same gift as your friend. GiftOZone is a network that allows you to see what gifts everyone is bringing.</p>
			</div>
			<div class="col-md-4">
				<h2>Always</h2>
				<p>Remember what you got last year, and the year before that. GiftOZone holds a history of all your past gifts in an easy to find way.</p>
			</div>
			<div class="col-md-4">
				<h2>Sometimes</h2>
				<p>Get that perfect gift they'll remember for the rest of their lives. Sorry, this one's up to you. :)</p>
			</div>
		</div>
		<hr/>
		<p>� 2013 AppYaks</p>
	</div>
</body>
</html>