function EventHandler() {

	var handlers = {};
	
	this.handleEvent = function (event) { // { id : id, object : object, operation : "D" }
		var handler = handlers[event.id];
		if (handler && handler.cbUpdate && event.operation === 'U') {
			handler.cbUpdate(event.object);
		} else if (handler && handler.cbDelete && event.operation === 'D') {
			handler.cbDelete(event.object);
		}
	};
	
	this.addHandler = function (id, cbUpdate, cbDelete) {
		
		//TODO: Support multiple handlers per id! i.e. we need to be able to re-render both Occasion and Calendar when the date changes 
		
		handlers[id] = { 
			id : id, 
			cbUpdate : cbUpdate, 
			cbDelete : cbDelete 
		};
	};
	
	this.removeHandler = function (id) {
		delete handlers[id];
	};
	
}