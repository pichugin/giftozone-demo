
/**
 * interval - time between synchronization calls in milliseconds;
 * */
function Synch(interval) {
	
	var refs = {};
	var self = this;

	var getRefsKeys = function() {
		return $.map(refs, function(value, key) { return { ref : key }; });
	};
	
	var executeCallback = function(ref, cb) {
		if (typeof cb === 'function') {
			cb(ref);
		}
	};
	
	var doSynch = function() {
		if (refs == null) {
			return;
		}
		var refsKeys = getRefsKeys();
		if (refsKeys.length === 0) {
			setTimeout(synch, interval);
			return;
		}
		$.ajax({
			type : "POST",
			url : "synch",
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			data : JSON.stringify(refsKeys),                           //[{ ref:"527b1cfdd6c466d882f9320a" }]), 
			success: function(data) {
				if (refs != null) {
					for (var i in data.synchList) {
						var synch = data.synchList[i];
						if (synch.operation === 'U') {
							executeCallback(synch.ref, refs[synch.ref].callbackUpdate);
						} else if (synch.operation === 'D') {
							executeCallback(synch.ref, refs[synch.ref].callbackDelete);
							self.removeRef(synch.ref);
						} 
					}
					setTimeout(doSynch, interval);
				}
			} 
		});
	};
	
	setTimeout(doSynch, interval);
	
	this.addRef = function(ref, callbackUpdate, callbackDelete) {
		refs[ref] = { 
			id : ref, 
			callbackUpdate : callbackUpdate, 
			callbackDelete : callbackDelete 
		};
	};
	
	this.removeRef = function(ref) {
		delete refs[ref];
	};
	
	this.stop = function() {
		refs = null;
	};
	
}
