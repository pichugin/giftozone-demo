/*
 * This file will observe the model and make changes to the DOM by use of templates.
 */

//====== Common ======

var storage = new Storage();
var model = new Model(storage);

$(document).ready(function() {
    window.onscroll = function() {
        var speed = 5.0;
        document.body.style.backgroundPosition = (-window.pageXOffset / speed) + "px " + (-window.pageYOffset / speed) + "px";
    };
    $.timeago.settings.allowFuture = true;
});

/*
var historyWrapper = new HistoryWrapper({
	"onOccasions" : function() {
		showOccasionsPage(true);
	},
	"onGiftBag" : function(params) {
		showGiftBagPage(params[0], params[1], true);
	},
	"onUserProfile" : function() {
		showUserProfilePage(true);
	}
}, window.history);
window.onpopstate = function(event) {
	historyWrapper.onPopState(event);
};
*/

function initSynchForRecentGiftBags() {
	//TODO: 
	/*
	var recentGiftBags = model.getRecentGiftBags();
	for (var i in recentGiftBags) {
		model.getCurrentPage().addHandler(recentGiftBags[i].id, null, function(giftBag) { 
			model.removeRecentGiftBag(giftBag.id);
			renderNavbarNav(recentGiftBags);
		});
	}
	*/
}

function resetPage() {
	$("#headerContainer").html("");
	$("#leftContainer1").html("");
	$("#leftContainer2").html("");
	$("#leftContainer3").html("");
	$("#centerContainer1").html("");
	$("#centerContainer2").html("");
}

function renderNavbarNav(recentGiftBags) {
	/*
	$("#navbarNavContainer").css("height", "0px");
	$("#navbarNavContainer").removeClass("in");

	model.getOccasions(function(occasions) {
		var occasionsMap = {};
		for (var i=0; i<occasions.length; i++) {
			occasionsMap[occasions[i].id] = occasions[i];
		}
		
		var html = new EJS({url: "ejs/navbarNav.ejs"}).render({"recentGiftBags" : recentGiftBags, "occasionsMap" : occasionsMap});
		$("#navbarNavContainer").html(html);
	});
	*/
}

function renderNavbarUser(userAccount) {
	var html = new EJS({url: "ejs/navbarUser.ejs"}).render({"userAccount" : userAccount});
	$("#navbarUserContainer").html(html);
}

function openFeedbackFormModal() {
	model.getCurrentUser(function(user) {
		model.getCurrentGiftBag(function(giftBag) {
			var html = new EJS({url: "ejs/feedbackFormModal.ejs"}).render({"userAccount" : user, "giftBag" : giftBag});
			$("#feedbackFormModalContainer").html(html);
			$('#feedbackFormModal').modal();
			$("#feedbackFormModal").on('shown', function () {
				$("#feedbackFormSeverity").focus();
			});
		});
	});
}

function closeFeedbackFormModal() {
	$('#feedbackFormModal').modal("hide");
}

//TODO: Format errors in a human-readable way:
function showError(origin, request, status, error) {
	if ("Forbidden" == error) { 
		//TODO: This message is shown twice. Wtf?
		alert("Oops! Looks like your session has expired.");
		window.location.reload();
	} else {
		alert("Origin: " + origin + "\nStatus: " + status + "\nError: " + error + "\nResponse: " + JSON.stringify(request.responseJSON));
	}
}

function showConfirmModal(message, okFunction) {
	var html = new EJS({url: "ejs/confirmModal.ejs"}).render({"message" : message});
	$("#confirmModalContainer").html(html);
	$("#confirmModalOk").click(function() {
		okFunction();
		closeConfirmModal();
		return false;
	});
	$('#confirmModal').modal();
	$("#confirmModal").on('shown', function () {
		$("#confirmModalOk").focus();
	});
}

function closeConfirmModal() {
	$('#confirmModal').modal("hide");
}



//====== Login Page ======

//TODO: Move this function somewhere:
function submitLogin(submitButton) {
	$.ajax({
		type : "POST", 
		url : "j_spring_security_check", 
		data : $(submitButton).closest("form").serialize(), 
		dataType : "json", 
		success: function(data) {
			var auth = data;
			if (auth.success) {
				window.location.reload(false); //reload using cached resources
			} else {
				showLoginError(auth.message);
			}			
		},
		error: function(request, status, error) {
			alert("TODO: Handle error...");
		}
	});
}

function showLoginError(message) {
	var html = new EJS({url: "ejs/errorMessage.ejs"}).render({"message" : message});
	$("#signInErrorContainer").html(html);
}



//====== Occasions Page ======

function showOccasionsPage(avoidHistory) {
	var occasionsPage = new OccasionsPage();
	model.setCurrentPage(occasionsPage);
	resetPage();
	occasionsPage.render();
}



//====== Gift Bag Page ======

function showGiftBagPage(occasionId, giftBagId, avoidHistory) {
	var giftBagPage = new GiftBagPage();
	model.setCurrentPage(giftBagPage);
	resetPage();
	giftBagPage.render(occasionId, giftBagId);

	initSynchForRecentGiftBags();
}



//====== User Profile Page ======

function showUserProfilePage(avoidHistory, userId) {
	var userProfilePage = new UserProfilePage();
	model.setCurrentPage(userProfilePage);
	resetPage();
	userProfilePage.render(userId);
	
	if (!avoidHistory) {
		historyWrapper.push("onUserProfile", "#?userProfile");
	}
}



//====== About Page ======

function showAboutPage() {
	var aboutPage = new AboutPage();
	model.setCurrentPage(aboutPage);
	resetPage();
	aboutPage.render();
}
