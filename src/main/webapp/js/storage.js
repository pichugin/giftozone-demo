/*
 * This file will provide all server-related interactions.
 */

//TODO: Do not define as a global:
function Storage() {
	
	var storage = {};
	
	//====== Get ======
	
	this.getCurrentUser = function(cb) {
		$.getJSON("user/current", function(data) {
			cb(data);
		}).error(function(request, status, error) {
			showError("getCurrentUser", request, status, error);
		});
	};
	
	this.getUserContacts = function(userId, cb) {
		$.getJSON("api/user/" + userId + "/contacts", function(data) {
			cb(data);
		}).error(function(request, status, error) {
			showError("getUserContacts", request, status, error);
		});
	};
	
	this.getUser = function(userId, cb) {
		$.getJSON("api/user/" + userId, function(data) {
			cb(data);
		}).error(function(request, status, error) {
			showError("getUser", request, status, error);
		});
	};
	
	this.getInvitations = function(cb) {
		$.getJSON("api/invitations", function(data) {
			cb(data);
		}).error(function(request, status, error) {
			showError("getInvitations", request, status, error);
		});
	};

	this.getOccasions = function(cb, paginationParams) {
		$.getJSON("api/occasions", paginationParams, function(data) {
			cb(data);
		}).error(function(request, status, error) {
			showError("getOccasions", request, status, error);
		});
	};

	this.getOccasion = function(occasionId, successFunction) {
		$.getJSON("api/occasions/" + occasionId, function(data) {
			occasion = data.occasion;
			if (successFunction) {
				successFunction(occasion);
			}
		}).error(function(request, status, error) {
			showError("getOccasion", request, status, error);
		});
	};

	this.getGiftBag = function(occasionId, giftBagId, cb) {
		$.getJSON("api/occasions/" + occasionId + "/giftBags/" + giftBagId, function(data) {
			cb(data);
		}).error(function(request, status, error) {
			showError("getGiftBag", request, status, error);
		});
	};



	//====== Update/Delete ======
	
	this.updateOccasion = function(occasion, cb) {
		$.ajax({
			type : "POST", 
			url : "api/occasions/" + (occasion.id ? occasion.id : ""), 
			contentType : "application/json; charset=utf-8", 
			dataType : "json", 
			data : JSON.stringify(occasion), 
			success: function(data) {
				if (!occasion.id) {
					/*
					 * TODO: This doesn't work:
					 */
					//storage.updateGiftBag({"occasionId" : data.occasion.id});
				}
				//TODO: Occasion's Gift Bags are cleared at this point:
				//model.updateOccasion(data.occasion.id, data.occasion);
				//TODO: Create new occasion. Calendar is not updated:
				if (cb) {
					cb(data.occasion);
				}
			},
			error: function(request, status, error) {
				showError("updateOccasion", request, status, error);
			}
		});
	};

	this.deleteOccasion = function(occasionId, cb) {
		$.ajax({
			type : "DELETE", 
			url : "api/occasions/" + occasionId, 
			success: function(data) {
				if (cb) {
					cb();
				}
			},
			error: function(request, status, error) {
				showError("deleteOccasion", request, status, error);
			}
		});
	};

	this.updateGiftBag = function(giftBag, cb) {
		$.ajax({
			type : "POST", 
			url : "api/occasions/" + giftBag.occasionId + "/giftBags/" + (giftBag.id ? giftBag.id : ""), 
			contentType : "application/json; charset=utf-8", 
			dataType : "json", 
			data : JSON.stringify(giftBag), 
			success: function(data) {
				cb(data);
			},
			error: function(request, status, error) {
				showError("updateGiftBag", request, status, error);
			}
		});
	};

	this.deleteGiftBag = function(occasionId, giftBagId, cb) {
		$.ajax({
			type : "DELETE", 
			url : "api/occasions/" + occasionId + "/giftBags/" + giftBagId, 
			success: function(data) {
				if (cb) {
					cb();
				}
			},
			error: function(request, status, error) {
				showError("deleteGiftBag", request, status, error);
			}
		});
	};

	this.inviteGiftBagParticipants = function(giftBag, inviteeEmail, cb) {
		$.ajax({
			type : "POST", 
			url : "api/invitations", 
			contentType : "application/json; charset=utf-8", 
			dataType : "json", 
			data : JSON.stringify({ "occasionId" : giftBag.occasionId, "giftBagId" : giftBag.id, "email" : inviteeEmail }), 
			success: function(data) {
				if (cb) {
					cb(data);
				}
			},
			error: function(request, status, error) {
				showError("inviteGiftBagParticipants", request, status, error);
			}
		});
	};

	this.acceptInvitationToGiftBag = function(invitationId, successFunction) {
		$.ajax({
			type : "POST", 
			url : "api/invitations/" + invitationId, 
			success: function(data) {
				if (successFunction) {
					successFunction();
				}
				//TODO: ??
				showOccasionsPage();
			},
			error: function(request, status, error) {
				showError("acceptInvitationToGiftBag", request, status, error);
			}
		});
	};

	this.updateGiftProposal = function(giftBag, giftProposal, cb) {
		$.ajax({
			type : "POST", 
			url : "api/occasions/" + giftBag.occasionId + "/giftBags/" + giftBag.id + "/giftProposals/" + (giftProposal.id ? giftProposal.id : ""), 
			contentType : "application/json; charset=utf-8", 
			dataType : "json", 
			data : JSON.stringify(giftProposal), 
			success: function(data) {
				cb(data);
			},
			error: function(request, status, error) {
				showError("updateGiftProposal", request, status, error);
			}
		});
	};

	this.deleteGiftProposal = function(giftBag, giftProposalId, cb) {
		$.ajax({
			type : "DELETE", 
			url : "api/occasions/" + giftBag.occasionId + "/giftBags/" + giftBag.id + "/giftProposals/" + giftProposalId, 
			success: function(data) { 
				cb(data);
			},
			error: function(request, status, error) {
				showError("deleteGiftProposal", request, status, error);
			}
		});
	};

	this.voteForGiftProposal = function(giftBag, giftProposalId, cb) {
		$.ajax({
			type : "POST", 
			url : "api/occasions/" + giftBag.occasionId + "/giftBags/" + giftBag.id + "/giftProposals/" + giftProposalId + "/voters", 
			dataType : "json", 
			success: function(data) {
				cb(data);
			},
			error: function(request, status, error) {
				showError("addGiftProposalVote", request, status, error);
			}
		});
	};

	this.unvoteForGiftProposal = function(giftBag, giftProposalId, userAccount, cb) {
		$.ajax({
			type : "DELETE", 
			url : "api/occasions/" + giftBag.occasionId + "/giftBags/" + giftBag.id + "/giftProposals/" + giftProposalId + "/voters/" + userAccount.userId, 
			dataType : "json", 
			success: function(data) {
				cb(data);
			},
			error: function(request, status, error) {
				showError("addGiftProposalVote", request, status, error);
			}
		});
	};

	this.updateGiftBagComment = function(giftBag, comment, cb) {
		$.ajax({
			type : "POST", 
			url : "api/occasions/" + giftBag.occasionId + "/giftBags/" + giftBag.id + "/comments/" + (comment.id ? comment.id : ""), 
			contentType : "application/json; charset=utf-8", 
			dataType : "json", 
			data : JSON.stringify(comment), 
			success: function(data) {
				cb(data);
			},
			error: function(request, status, error) {
				showError("updateGiftBagComment", request, status, error);
			}
		});
	};

	//TODO: This doesn't work!
	this.updateUser = function(userAccount, cb) {
		var u = userAccount;
		$.ajax({
			type : "POST", 
			url : "api/user/" + userAccount.userId, 
			contentType : "application/json; charset=utf-8", 
			dataType : "json",
			//TODO: The following is a temporary solution. We shouldn't bring all user data from the server in the first place:
			data : JSON.stringify({id:u.id, userId:u.userId, displayName:u.displayName, imageUrl:u.imageUrl, webSite:u.webSite, email:u.email}), //JSON.stringify(userAccount), 
			success: function(data) {
				if (cb) {
					cb(data);
				}
			},
			error: function(request, status, error) {
				showError("updateUser", request, status, error);
			}
		});
	};

	this.submitFeedbackForm = function(feedbackForm, successFunction) {
		
		//TODO:
		alert("TODO: Submit: " + JSON.stringify(feedbackForm));
		
		if (successFunction) {
			successFunction();
		}
	};
	
};
