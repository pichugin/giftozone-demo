
function HistoryWrapper(handlers, history) {
	
	this.handlers = handlers || {};
	this.history = history || window.history;
	
	this.addHandler = function(key, func) {
		handlers[key] = func;
	};
	
	this.onPopState = function(event) {
		if (event.state && event.state.handler) {
			if (typeof this.handlers[event.state.handler] === "function") {
				this.handlers[event.state.handler](event.state.params);
			}
		}
	};
	
	this.push = function(handlerName, url, params) {
		this.history.pushState({ handler : handlerName, params : params }, null, url);
	};

}