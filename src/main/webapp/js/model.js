/*
 * This file will store all of the state/data in the application and query/write to the storage.
 */

function Model(storageParam, synchParam, eventHandlersParam) {
	
	var storage = storageParam;
	var user = null;
	var userContacts = null;
	var contactEmails = new Set();
	var invitations = null;
	var occasions = null;
	var recipientNames = new Set();
	var occasionDescriptions = new Set();
	var currentGiftBag = null;
	var recentGiftBags = []; //TODO: Use Set instead of array.
	
	var synch = synchParam || new Synch(15000);
	var eventHandlers = eventHandlersParam || {};
	var currentPage = null;
	
	var self = this;
	
	this.setCurrentPage = function (page) { currentPage = page; };
	this.getCurrentPage = function () { return currentPage; };
	
	function sendEventToPage(id, object, operation) {
		if (currentPage) {
			if (typeof currentPage.handleEvent === 'function') {
				currentPage.handleEvent({ id : id, object : object, operation : operation });
			}
		}
	} 
	
	this.getCurrentUser = function(cb) {
		if (user == null) {
			initUser(cb);
		} else {
			cb(user);
		}
	};
	
	function initUser(cb) {
		storage.getCurrentUser(function(data) {
			user = data.user;
			synch.addRef(user.userId, function() {
				storage.getCurrentUser(function(data) {
					user = data.user;
					sendEventToPage(user.userId, user, 'U');
				});
			});
			if (cb) {
				cb(user); 
			}
		});
	}
	
	this.updateUser = function(updatedUser, cb) {
		storage.updateUser(updatedUser, function(data) {
			user = data.userAccount;
			cb();
		});
	};
	
	this.getUser = function(userId, cb) {
		storage.getUser(userId, function(data) {
			cb(data.userBrief);
		});
	};
	
	this.getUserContact = function(contactUserId, cb) {
		var localCb = function() {
			cb(userContacts[contactUserId]);
		};
		if (userContacts == null) {
			initUserContacts(user.userId, localCb);
		} else {
			localCb();
		}
	};
	
	this.getUserContacts = function(userId, cb) {
		var localCb = function() {
			cb($.map(userContacts, function(obj, indx) { return obj; }));
		};
		if (userContacts == null) {
			initUserContacts(userId, localCb);
		} else {
			localCb();
		}
	};
	
	/*
	 * TODO: We need to make sure this only gets called once per page load.
	 * Curently the occasions page calls this (yes, init, not get) about 30 
	 * times when the page loads. We need a mechanism such that if an init
	 * method is currently executing, all subsequent calls are blocked.
	 */
	function initUserContacts(userId, cb) {
		storage.getUserContacts(userId, function(data) {
			userContacts = {};
			contactEmails = new Set();
			for (var i=0; i<data.userBriefList.length; i++) {
				var contact = data.userBriefList[i];
				
				//Save contacts in a map
				userContacts[contact.userId] = contact;
				
				//Init contact emails for autocomplete
				contactEmails.add(contact.email);
			}
			cb();
		});
	}

	/*
	 * TODO: This should probably be asynchronous, triggering getUserContact
	 * if userContacts is null. However, it's called from the EJS pages
	 * directly and it's not easy to use callbacks there, so this will do
	 * for now.
	 */
	this.contactIdToName = function(contactUserId) {
		if (userContacts == null) {
			return contactUserId;
		} else {
			var contact = userContacts[contactUserId];
			if (!contact) {
				return "unknown user (" + contactUserId + ")";
			}
			return (contact.displayName) ? contact.displayName : contact.userId;
		}
	};
	
	this.getContactEmails = function() {
		return contactEmails.toArray();
	};

	this.getInvitations = function(cb) {
		if (invitations == null) {
			initInvitations(cb);
		} else {
			cb(invitations);
		}
	};

	function initInvitations(cb) {
		storage.getInvitations(function(data) {
			invitations = data.invitationList;
			cb(invitations);
		});
	}

	this.getOccasions = function(cb) {
		var localCb = function() {
			cb(occasions);
		};
		if (occasions == null) {
			initOccasions(localCb);
		} else {
			localCb();
		}
	};
	
	this.getNewerOccasions = function(cb) {
		var paginationParams = occasions == null ? {} : {
			dateInMillis : occasions[0].dateInMillis,
			direction : "UP",
			limit : 2
		};
		storage.getOccasions(function(data) {
			occasions = $.merge(data.occasionList.reverse(), occasions || []);
			//TODO: Add received occasions to the synch
			cb();
		}, paginationParams);
	};

	this.getOlderOccasions = function(cb) {
		var paginationParams = occasions == null ? {} : {
			dateInMillis : occasions[occasions.length - 1].dateInMillis,
			direction : "DOWN",
			limit : 2
		};
		storage.getOccasions(function(data) {
			occasions = (occasions || []).concat(data.occasionList);
			//TODO: Add received occasions to the synch
			cb();
		}, paginationParams);
	};

	function initOccasions(cb) {
		occasions = []; //{};
		recipientNames = new Set();
		occasionDescriptions = new Set();
		storage.getOccasions(function(data) {
			for (var i=0; i<data.occasionList.length; i++) {
				//Save occasions in a map
				var occasion = data.occasionList[i];
				occasions.push(occasion);
				
				//Init recipients, descriptions lists for autocomplete
				recipientNames.add(occasion.recipient);
				occasionDescriptions.add(occasion.description);
				
				synch.addRef(occasion.id, function(occId) {
					storage.getOccasion(occId, function(occ) {
						occasions[getOccasionIndex(occ.id)] = occ; //TODO: Check if index != -1
						sendEventToPage(occ.id, occ, 'U');
					});
				});
			}
			cb();
		}, { limit : 2 });  //This limit is temporarily ignored on the server, so all occasions get brought back
	}
	
	function getOccasionIndex(occasionId) {
		for (var i in occasions) {
			if (occasions[i].id === occasionId) {
				return i;
			}
		}
		return -1; // This can be error prone (e.g. occasions[-1] = occasion; // is a valid statement)
	}
	
	function insertOccasionByDate(occasion) {
		for (var i in occasions) {
			var occ = occasions[i];
			if (occ.dateInMillis < occasion.dateInMillis) {
				occasions.splice(i, 0, occasion);
				return;
			}
		}
		occasions.push(occasion);
	}
	
	this.getOccasion = function(occasionId, cb) {
		if (occasions == null) {
			initOccasions(cb);
		} else {
			cb(occasions[getOccasionIndex(occasionId)]);
		}
	};
	
	this.updateOccasion = function(occasion, cb) {
		storage.updateOccasion(occasion, function(updatedOccasion) {
			// If the date was changed the occasion's position in the list may change:
			if (occasion.id) {
				// Remove it from the old position:
				occasions.splice(getOccasionIndex(occasion.id), 1);
			}
			insertOccasionByDate(updatedOccasion);
			cb();
		});
	};
	
	this.deleteOccasion = function(occasionId, cb) {
		storage.deleteOccasion(occasionId, function() {
			occasions.splice(getOccasionIndex(occasionId), 1);
			synch.removeRef(occasionId);
			cb();
		});
	};
	
	this.getRecipientNames = function() {
		return recipientNames.toArray();
	};
	
	this.getOccasionDescriptions = function() {
		return occasionDescriptions.toArray();
	};
	
	this.getCurrentGiftBag = function(cb) {
		cb(currentGiftBag);
	};
	
	this.initCurrentGiftBag = function(occasionId, giftBagId, cb) {
		storage.getGiftBag(occasionId, giftBagId, function(data) {
			currentGiftBag = data.giftBag;
			addRecentGiftBag(currentGiftBag);
			if (cb) {
				synch.addRef(giftBagId, function() { 
					self.initCurrentGiftBag(occasionId, giftBagId); 
				});
				cb(currentGiftBag);
			} else {
				sendEventToPage(giftBagId, currentGiftBag, 'U');
			}
		});
	};
	
	this.updateGiftBag = function(newGiftBag, cb) {
		storage.updateGiftBag(newGiftBag, function(data) {
			//Update "current" gift bag
			giftBag = data.giftBag;

			//Update gift bag in occasions list
			var parentOccasion = occasions[getOccasionIndex(giftBag.occasionId)];
			if (parentOccasion.giftBags && parentOccasion.giftBags.length > 0) {
				var updated = false;
				for (var i=0; i<parentOccasion.giftBags.length; i++) {
					if (parentOccasion.giftBags[i].id == giftBag.id) {
						parentOccasion.giftBags[i] = giftBag;
						updated = true;
						break;
					}
				}
				if (!updated) {
					parentOccasion.giftBags.push(giftBag);
				}
			} else {
				parentOccasion.giftBags = [];
				parentOccasion.giftBags.push(giftBag);
			}
			
			//TODO: Do synch here
			
			cb(giftBag);
		});
	};
	
	this.deleteGiftBag = function(occasionId, giftBagId, cb) {
		storage.deleteGiftBag(occasionId, giftBagId, function() {
			var occasion = occasions[getOccasionIndex(occasionId)];
			for (var i in occasion.giftBags) {
				if (occasion.giftBags[i].id == giftBagId) {
					occasion.giftBags.splice(i, 1);
					break;
				}
			}
			synch.removeRef(giftBagId);
			cb();
		});
	};
	
	this.inviteGiftBagParticipants = function(inviteeEmail) {
		storage.inviteGiftBagParticipants(currentGiftBag, inviteeEmail, function(data) {
			//TODO:
			alert(JSON.stringify(data));
		});
	};
	
	//TODO: Consider a map implementation to speed this up:
	this.getGiftProposal = function(giftProposalId) {
		for (var i in currentGiftBag.giftProposals) {
			if (currentGiftBag.giftProposals[i].id == giftProposalId) {
				return currentGiftBag.giftProposals[i];
			}
		}
		return null;
	};
	
	this.saveGiftProposal = function(giftProposal, cb) {
		storage.updateGiftProposal(currentGiftBag, giftProposal, function(data) {
			updateGiftProposal(data.giftProposal);
			cb(currentGiftBag);
		});
	};
	
	function updateGiftProposal(giftProposal) {
		for (var i in currentGiftBag.giftProposals) {
			if (currentGiftBag.giftProposals[i].id == giftProposal.id) {
				currentGiftBag.giftProposals[i] = giftProposal;
				updateOccasionWithGiftBag(currentGiftBag);
				return;
			}
		}
		if (!currentGiftBag.giftProposals) {
			currentGiftBag.giftProposals = [];
		}
		currentGiftBag.giftProposals.push(giftProposal);
		updateOccasionWithGiftBag(currentGiftBag);
	}
	
	function updateOccasionWithGiftBag(updatedGiftBag) {
		var occ = occasions[getOccasionIndex(currentGiftBag.occasionId)];
		if (occ) {
			for (var i in occ.giftBags) {
				var giftBag = occ.giftBags[i];
				if (giftBag.id === updatedGiftBag.id) {
					occ.giftBags[i] = updatedGiftBag;
					return;
				}
			}
			occ.giftBags.push(updatedGiftBag);
		}
	}
	
	this.deleteGiftProposal = function(giftProposalId, cb) {
		storage.deleteGiftProposal(currentGiftBag, giftProposalId, function(data) {
			deleteGiftProposalFromCurrent(giftProposalId);
			updateOccasionWithGiftBag(currentGiftBag);
			cb(currentGiftBag);
		});
	};
	
	function deleteGiftProposalFromCurrent(giftProposalId) {
		for (var i in currentGiftBag.giftProposals) {
			if (currentGiftBag.giftProposals[i].id == giftProposalId) {
				currentGiftBag.giftProposals.splice(i, 1);
				return;
			}
		}
	}
	
	this.voteForGiftProposal = function(proposalId, cb) {
		storage.voteForGiftProposal(currentGiftBag, proposalId, function(data) {
			setGiftProposalById(data.giftProposal);
			cb(currentGiftBag);
		});
	};
	
	this.unvoteForGiftProposal = function(proposalId, cb) {
		storage.unvoteForGiftProposal(currentGiftBag, proposalId, user, function(data) {
			setGiftProposalById(data.giftProposal);
			cb(currentGiftBag);
		});
	};
	
	function setGiftProposalById(giftProposal) {
		if (currentGiftBag.giftProposals) {
			for (var i in currentGiftBag.giftProposals) {
				if (currentGiftBag.giftProposals[i].id == giftProposal.id) {
					currentGiftBag.giftProposals[i] = giftProposal;
					return;
				}
			}
		}
	}
	
	this.addGiftBagComment = function(comment, cb) {
		storage.updateGiftBagComment(currentGiftBag, comment, function(data) {
			updateGiftBagComment(data.comment);
			cb(currentGiftBag);
		});
	};
	
	function updateGiftBagComment(comment) {
		if (currentGiftBag.comments) {
			for (var i in currentGiftBag.comments) {
				if (currentGiftBag.comments[i].id == comment.id) {
					currentGiftBag.comments[i] = comment;
					return;
				}
			}
		} else {
			currentGiftBag.comments = [];
		}
		currentGiftBag.comments.push(comment);
	}
	
	this.getRecentGiftBags = function() {
		return recentGiftBags;
	};
	
	function addRecentGiftBag(giftBag) {
		removeRecentGiftBag(giftBag.id);
		//Insert at first index:
		recentGiftBags.unshift(giftBag);
	};
	
	function removeRecentGiftBag(giftBagId) {
		//Remove if already exists:
		var i = recentGiftBags.length;
		while (i--) {
			if (recentGiftBags[i].id == giftBagId) {
				recentGiftBags.splice(i, 1);
			}
		}
	};

};