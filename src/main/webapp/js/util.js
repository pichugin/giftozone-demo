// Common utility functions shared between pages

$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};



function Set() {
	this.objects = [];
}

Set.prototype.add = function(object) {
	if (!this.contains(object)) {
		this.objects.push(object);
	}
};

Set.prototype.contains = function(object) {
	for (var i=0; i<this.objects.length; i++) {
		if (this.objects[i] === object) {
			return true;
		}
	}
	return false;
};

Set.prototype.toArray = function() {
	return this.objects;
};



function escapeQuotes(str) {
	return str ? str.replace('"', '&quot;') : str;
}