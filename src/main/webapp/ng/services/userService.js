angular.module('giftozone.services').factory("UserService", function($http, ErrorHandler) {
	return {
		getCurrentUser : function(cb) {
			$http.get("user/current").success(function(data) {
				cb(data.user);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		},
		getUser : function(userId, cb) {
			$http.get("api/user/" + userId).success(function(data) {
				cb(data.userBrief);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		},
		updateUser : function(user, cb) {
			$http.post("api/user/" + user.userId, user).success(function(data) {
				cb(data.user);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		},
		getUserContacts : function(userId, cb) {
			$http.get("api/user/" + userId + "/contacts").success(function(data) {
				cb(data.userBriefList);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		},
        uploadAvatar : function(file, userId, fieldName, cb) {
            var data = new FormData();
            data.append(fieldName, file);
			$http({
				method : "POST", 
				url : 'api/user/' + userId + '/image', 
				data : data,
	            transformRequest : angular.identity,
	            headers : { 'Content-Type': undefined }
	        }).success(cb).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
        },
        addRecentGiftBag : function(giftBag) {
        	//TODO
        },
        getRecentGiftBags : function(cb) {
        	//TODO
        	cb([{"id" : "52eef2230364dbc6c2ad24ec", "occasionId" : "52eef04903643808ea51eeb4"}]);
        }
	}
});