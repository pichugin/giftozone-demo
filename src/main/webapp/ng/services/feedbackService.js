angular.module('giftozone.services').factory("FeedbackService", function($http, ErrorHandler, Utils) {
	return {
		sendFeedback : function(feedback, cb) {
			$http.post("api/feedback", feedback).success(function(data) {
				Utils.callSafe(cb);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		}
	};
});