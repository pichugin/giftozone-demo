angular.module('giftozone.services').factory("OccasionService", function($http, ErrorHandler) {
	return {
		getOccasions : function(cb) {
			$http.get("api/occasions").success(function(data) {
				cb(data.occasionList);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		},
		getOccasion : function(occasionId, cb) {
			$http.get("api/occasions/" + occasionId).success(function(data) {
				cb(data.occasion);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		},
		updateOccasion : function(occasion, cb) {
			$http.post("api/occasions/" + (occasion.id ? occasion.id : ""), 
					{"id" : occasion.id, 
					"description" : occasion.description, 
					"recipient" : occasion.recipient, 
					"date" : occasion.date}
			).success(function(data) {
				cb(data.occasion);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		},
		deleteOccasion : function(occasionId, cb) {
			$http.delete("api/occasions/" + occasionId).success(function(data) {
				if (typeof(cb) === 'function') {
					cb();
				}
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		}
	}
});