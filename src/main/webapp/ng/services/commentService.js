angular.module('giftozone.services').factory("CommentService", function($http, ErrorHandler, Utils) {
	return {
		updateComment : function(comment, cb) {
			$http.post("api/comments/" + (comment.id ? comment.id : ""), comment).success(function(data) {
				Utils.callSafe(cb, data.comment);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		},
		deleteComment : function(commentId, cb) {
			$http.delete("api/comments/" + commentId).success(function(data) {
				Utils.callSafe(cb);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		}
	};
});