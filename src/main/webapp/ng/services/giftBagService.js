angular.module('giftozone.services').factory("GiftBagService", function($http, ErrorHandler, Utils) {
	return {
		getGiftBag : function(occasionId, giftBagId, cb) {
			$http.get("api/occasions/" + occasionId + "/giftBags/" + giftBagId).success(function(data) {
				cb(data.giftBag);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		},
		updateGiftBag : function(giftBag, cb) {
			$http.post("api/occasions/" + giftBag.occasionId + "/giftBags/" + (giftBag.id ? giftBag.id : ""), giftBag).success(function(data) {
				cb(data.giftBag);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		},
		updateGiftProposal : function(occasionId, giftBagId, giftProposal, cb) {
			$http.post("api/occasions/" + occasionId + "/giftBags/" + giftBagId + "/giftProposals/" + (giftProposal.id ? giftProposal.id : ""), giftProposal).success(function(data) {
				cb(data.giftProposal);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		},
		voteForGiftProposal : function(occasionId, giftBagId, giftProposalId, cb) {
			$http.post("api/occasions/" + occasionId + "/giftBags/" + giftBagId + "/giftProposals/" + giftProposalId + "/voters").success(function(data) {
				cb(data.giftProposal);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		},
		unvoteForGiftProposal : function(occasionId, giftBagId, giftProposalId, userId, cb) {
			$http.delete("api/occasions/" + occasionId + "/giftBags/" + giftBagId + "/giftProposals/" + giftProposalId + "/voters/" + userId).success(function(data) {
				cb(data.giftProposal);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		},
		deleteGiftProposal : function(occasionId, giftBagId, giftProposalId, cb) {
			$http.delete("api/occasions/" + occasionId + "/giftBags/" + giftBagId + "/giftProposals/" + giftProposalId).success(function() {
				cb();
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		},
		deleteGiftBag : function(occasionId, giftBagId, cb) {
			$http.delete("api/occasions/" + occasionId + "/giftBags/" + giftBagId).success(function(data) {
				Utils.callSafe(cb);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		}
	}
});