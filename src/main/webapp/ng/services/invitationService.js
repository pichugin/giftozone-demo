angular.module('giftozone.services').factory("InvitationService", function($http, ErrorHandler) {
	return {
		inviteGiftBagParticipant : function(occasionId, giftBagId, inviteeEmail, suggestCurrentGiftBag, cb) {
			var invitation = { 
				"occasionId" : occasionId,
				"email" : inviteeEmail
			};
			if (suggestCurrentGiftBag) {
				invitation.giftBagId = giftBagId;
			}
			$http.post("api/invitations", invitation).success(function(data) {
				cb(data.invitation);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		},
		// Get invitees to this gift bag:
		getGiftBagInvitees : function(occasionId, giftBagId, cb) {
			$http.get("api/invitations/occasions/" + occasionId + "/giftBags/" + giftBagId).success(function(data) {
				cb(data.invitationList);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		},
		// Get all invitations sent to me:
		getInvitations : function(cb) {
			$http.get("api/invitations").success(function(data) {
				cb(data.invitationList);
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		},
		acceptInvitationToGiftBag : function(invitationId, cb) {
			$http.post("api/invitations/" + invitationId
			).success(function(data) {
				cb();
			}).error(function(data, status, headers, config) {
				ErrorHandler.error(data, status, headers, config);
			});
		}
	}
});