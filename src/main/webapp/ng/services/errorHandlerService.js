angular.module('giftozone.services').service("ErrorHandler", function($window) {
	return {
		error : function(data, status, headers, config) {
			$window.alert("Error!" + (status ? (" (" + status + ")") : ""));
			if (console) {
				console.log("Error! (" + status + ")");
				if (config) {
					console.log("  Request: " + JSON.stringify(config));
				}
			}
		}
	}
});