angular.module('giftozone.services').factory("Utils", function() {
	return {
		getIndexById : function(objects, id) {
			for (var index in objects) {
				if (objects[index].id === id) {
					return parseInt(index);
				}
			}
			return -1;
		},
		stringToDate_YYYYMMDD : function(str) {
			if (!str) {
				return null;
			}
			var dateTokens = str.split("-");
			return new Date(dateTokens[0], dateTokens[1] - 1, dateTokens[2]);
		},
		dateToString_MMDDYYYY : function(date) {
			if (!date) {
				return "";
			}
			return addZeroPadding(date.getMonth() + 1) + "-" + addZeroPadding(date.getDate()) + "-" + date.getFullYear();
		},
		dateToString_YYYYMMDD : function(date) {
			if (!date) {
				return "";
			}
			return date.getFullYear() + "-" + addZeroPadding(date.getMonth() + 1) + "-" + addZeroPadding(date.getDate());
		},
		callSafe : function(func, param) {
			if (typeof(func) === 'function') {
				func(param);
			}
		}
	}
	
	function addZeroPadding(num) {
		return ('0' + num).slice(-2);
	}
});