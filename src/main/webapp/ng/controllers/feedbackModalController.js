//TODO: figure out how to create this within the giftozone controllers module, not in the global scope:
var FeedbackModalCtrl = function ($scope, $modalInstance, userId, occasionId, giftBagId, FeedbackService) {
	$scope.feedback = {userId : userId, occasionId : occasionId, giftBagId : giftBagId};
	
	$scope.ok = function(feedback) {
		FeedbackService.sendFeedback(feedback, function() {
			alert("Thank you! Your feedback is much appreciated!");
		});
		$modalInstance.close();
	};
	
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
};