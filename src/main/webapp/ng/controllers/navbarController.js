angular.module('giftozone.controllers').controller("NavbarCtrl", function($scope, $modal, UserService) {
	
	UserService.getCurrentUser(function(user) {
		$scope.userAccount = user;
	});
	
	UserService.getRecentGiftBags(function(giftBags) {
		$scope.recentGiftBags = giftBags;
	});
	
	$scope.openFeedbackFormModal = function() {
		var modalInstance = $modal.open({
			templateUrl: 'partials/feedbackModal.html',
			controller: FeedbackModalCtrl,
			resolve: {
				userId : function() {
					return $scope.userAccount.userId;
				},
				occasionId : function() {
					//TODO: Pass in the correct value
					return "???";
				},
				giftBagId : function() {
					//TODO: Pass in the correct value
					return "???";
				}
			}
		});
	};
	
});