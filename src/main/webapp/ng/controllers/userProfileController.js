angular.module('giftozone.controllers').controller("UserProfileCtrl", function($scope, $routeParams, UserService) {
	
	UserService.getCurrentUser(function (user) {
		$scope.currentUser = user;
	});
	
	UserService.getUser($routeParams.userId, function(user) {
		$scope.userAccount = user;
	});
	
	$scope.userContacts = [];
    $scope.pageState = {};
    var oldUser;



    // Edit Avatar
    $scope.editAvatar = function() {
    	$scope.pageState.editAvatar = true;
    };
    
    $scope.saveEditAvatar = function() {
    	if ($scope.newAvatar) {
    		UserService.uploadAvatar($scope.newAvatar.file, $scope.userAccount.userId, $scope.newAvatar.fieldName, function() {
    			//TODO: The following line would change the image, but the consequent Save would fail due to a Bad Request:
    			//$scope.userAccount.tempImageUrl = 'api/user/' + $scope.currentUser.userId + '/image?a=' + new Date().getTime();;
    		});
    	}
    	$scope.newAvatar = null;
    	$scope.pageState.editAvatar = false;
    };

    $scope.cancelEditAvatar = function() {
    	$scope.newAvatar = null;
    	$scope.pageState.editAvatar = false;
    };
    
    $scope.locateAvatarFile = function(file, fieldName, maxSize, formats) {
    	console.log("locateAvatarFile(): file: " + file + "; fieldName:" + fieldName + "; max-size:" + maxSize + "; formats:" + formats);
    	//TODO: Validate format and size.
    	$scope.newAvatar = {};
    	$scope.newAvatar.file = file;
    	$scope.newAvatar.fieldName = fieldName;
    };
    
    $scope.isAvatarUnchanged = function() {
    	return $scope.newAvatar == null;
    };



    // Edit User Profile
	$scope.editUserProfile = function() {
        oldUser = angular.copy($scope.userAccount);
		$scope.pageState.editUserProfile = true;
	};

    $scope.saveEditUserProfile = function() {
    	UserService.updateUser($scope.userAccount, function(user) {
    		oldUser = null;
    		$scope.pageState.editUserProfile = false;
    	});
    };

    $scope.cancelEditUserProfile = function() {
        $scope.userAccount = oldUser;
        $scope.pageState.editUserProfile = false;
    };
    
    $scope.isUserUnchanged = function() {
    	return angular.equals($scope.userAccount, oldUser);
    };

});