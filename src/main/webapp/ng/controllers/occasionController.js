angular.module('giftozone.controllers').controller("OccasionListCtrl", 
		function($scope, $filter, $modal, $location, OccasionService, GiftBagService, UserService, InvitationService, Utils) {
	
	OccasionService.getOccasions(function(occasions) {
		setOccasions(occasions);
	});
	
	function setOccasions(occasions) {
		var currentTime = (new Date()).getTime();
		for (i=0; i<occasions.length; i++) {
			var occasionTime = Utils.stringToDate_YYYYMMDD(occasions[i].date).getTime();
			if (currentTime < occasionTime) {
				occasions.splice(i, 0, {nowSeparator : true});
				break;
			}
		}
		$scope.occasionList = occasions;
		renderOccasionsCalendar(occasions);
	}
	
	UserService.getCurrentUser(function(user) {
		$scope.userAccount = user;
		
		UserService.getUserContacts(user.userId, function(contacts) {
			$scope.userContacts = contacts;
		});
	});
	
	InvitationService.getInvitations(function(invitations) {
		$scope.invitationList = invitations;
	});
	
	
	
	function getOccasionById(occasionId) {
		for (var i=0; i<$scope.occasionList.length; i++) {
			if ($scope.occasionList[i].id == occasionId) {
				return $scope.occasionList[i];
			}
		}
		return null;
	}
	
	$scope.hasGiftProposals = function(giftBag) {
		return giftBag.giftProposals && giftBag.giftProposals.length > 0;
	};
	
	$scope.getGiftBagInvitation = function(occasionId, giftBagId) {
		for (var i=0; i < $scope.invitationList.length; i++) {
			var invitation = $scope.invitationList[i];
			if (invitation.occasionId == occasionId && invitation.giftBagId == giftBagId) {
				return invitation;
			}
		}
		return null;
	};
	
	$scope.acceptGiftBagInvitation = function(invitationId) {
		InvitationService.acceptInvitationToGiftBag(invitationId, function() {
			$scope.invitation = null;
		});
	}
	
	
	
	var occasionsCalendar;
	
	function renderOccasionsCalendar(occasionsList) {
		//Collect data
		var occasionsCalendarData = {};
		for (var i in occasionsList) {
			var occasion = occasionsList[i];
			var occasionDate = Utils.stringToDate_YYYYMMDD(occasion.date);
			var occasionDateStr = Utils.dateToString_MMDDYYYY(occasionDate);
			
			if (!occasionsCalendarData[occasionDateStr]) {
				//Ideally the value should be the html to display, but since we're not displaying it anyways, set it to the object
				occasionsCalendarData[occasionDateStr] = occasion;
			}
		}
		
		//Render calendar
		occasionsCalendar = $("#occasionsCalendarContainer").calendario({
			caldata : occasionsCalendarData,
			onDayClick : function( $el, $contentEl, dateProperties ) {
				var dateClicked = new Date(dateProperties.year, dateProperties.month - 1, dateProperties.day);
				
				//Coordinates where to show the context menu:
				//var divClicked = $($el[0]);
				//console.log(divClicked.offset().left + " - " + divClicked.offset().top);
				
				if( $contentEl.length > 0 ) {
					var occasion = occasionsCalendarData[Utils.dateToString_MMDDYYYY(dateClicked)];

					//Scroll to occasion
				    var dateBox = $("#occasion-date-box-" + occasion.id);
				    var scrollTime = 300;
				    $('html,body').animate({scrollTop: dateBox.offset().top - 80}, scrollTime);
				    
				    //Highlight date box
				    setTimeout(function() {
				    	flashObjectBackground(dateBox);
				    }, scrollTime / 2);
				} else {
					$scope.openCreateOccasionModal(dateClicked);
				}
			},
			displayWeekAbbr : true
		});
		occasionsCalendarRenderLabels();
	}

	function occasionsCalendarRenderLabels() {
		if (occasionsCalendar) {
			$("#occasionsCalendarMonth").html(occasionsCalendar.getMonthName());
			$("#occasionsCalendarYear").html(occasionsCalendar.getYear());
		}
	}

	$scope.occasionsCalendarPrev = function() {
		occasionsCalendar.gotoPreviousMonth();
		occasionsCalendarRenderLabels();
	};

	$scope.occasionsCalendarNext = function() {
		occasionsCalendar.gotoNextMonth();
		occasionsCalendarRenderLabels();
	};
	
	$scope.showTodayOnCalendar = function() {
		$scope.showDateOnCalendar(Utils.dateToString_YYYYMMDD(new Date()));
	}
	
	$scope.showDateOnCalendar = function(dateStr) {
		var date = Utils.stringToDate_YYYYMMDD(dateStr);
		occasionsCalendar.goto(date.getMonth(), date.getFullYear(), null);
		occasionsCalendarRenderLabels();
		
		//Highlight date box
		var calendarDateBox = $("#occasionsCalendarContainer span.fc-date").filter(function() {
				return $(this).text() == date.getDate();
			}).parent();
		flashObjectBackground(calendarDateBox);
	}
	
	function flashObjectBackground(object) {
    	var originalColor = object.css("background-color");
    	var originalBorderColor = object.css("border-color");
    	object.css("background-color", "#fff");
    	object.css("border-color", "#fff");
    	object.animate({ backgroundColor : originalColor, borderColor : originalBorderColor });
	}
	
	
	
	
	$scope.openCreateOccasionModal = function(date) {
		openEditOccasionModal({"date" : date}, function(occasion) {
			OccasionService.updateOccasion(occasion, function(occ) {
				OccasionService.getOccasions(function(occasions) {
					setOccasions(occasions);
				});
			});
		});
	};
	
	$scope.openEditOccasionModal = function(occasionId) {
		var occ = getOccasionById(occasionId);
		openEditOccasionModal(occ, function(occasion) {
			OccasionService.updateOccasion(occasion, function(occ) {
				OccasionService.getOccasions(function(occasions) {
					setOccasions(occasions);
				});
			});
		});
	};
	
	$scope.deleteOccasionConfirm = function(occasionId) {
		openConfirmModal("Are you sure you want to delete this occasion?", function() {
			OccasionService.deleteOccasion(occasionId, function() {
				// Remove occasion from the model after it was successfully deleted on the server:
				$scope.occasionList.splice(Utils.getIndexById($scope.occasionList, occasionId), 1);
				renderOccasionsCalendar($scope.occasionList);
			});
		});
	};
	
	function openEditOccasionModal(occ, cb) {
		var modalInstance = $modal.open({
			templateUrl: 'partials/occasions/editOccasionModal.html',
			controller: EditOccasionModalCtrl,
			resolve: {
				occasion : function() {
					return occ;
				}
			}
		});
	
		modalInstance.result.then(function(occasion) {
			cb(occasion);
		});
	}
	
	//TODO: Consider taking out to be reused by other controllers
	function openConfirmModal(msg, cb) {
		var modalInstance = $modal.open({
			templateUrl: 'partials/confirmModal.html',
			controller: ConfirmModalCtrl,
			resolve: {
				message : function() {
					return msg;
				}
			}
		});
	
		modalInstance.result.then(cb);
	}
	
});

//TODO: figure out how to create this within the giftozone controllers module, not in the global scope:
var EditOccasionModalCtrl = function($scope, $modalInstance, occasion) {
	$scope.occasion = angular.copy(occasion);
	
	$scope.ok = function() {
		$modalInstance.close($scope.occasion);
	};
	
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
};