//TODO: figure out how to create this within the giftozone controllers module, not in the global scope:
var ConfirmModalCtrl = function ($scope, $modalInstance, message) {
	$scope.message = message;
	
	$scope.ok = function() {
		$modalInstance.close();
	};
	
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
};