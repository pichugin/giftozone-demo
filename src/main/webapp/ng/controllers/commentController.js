angular.module('giftozone.controllers').controller("CommentCtrl", function($scope, CommentService, Utils) {
	
	var COMMENT_TYPE_GIFTBAG = 1; 

	$scope.newComment = {};
	$scope.replyingToComment;
	
	$scope.postComment = function(newComment, parent) {
		newComment.refId = $scope.giftBag.id;
		newComment.type = COMMENT_TYPE_GIFTBAG;
		var afterUpdate = function() { 
			$scope.resetReply(); 
		};
		var afterAdd = function(savedComment) {
			parent = parent || $scope.giftBag;
			if (!parent.comments) {
				parent.comments = [];
			}
			parent.comments.push(savedComment);
			$scope.resetReply();
		};
		CommentService.updateComment(newComment, newComment.id ? afterUpdate : afterAdd);
	};

	$scope.deleteComment = function(comment) {
		var commentId = comment.id;
		CommentService.deleteComment(commentId, function() {

			// Since we don't have a reference to the parent we have to trace the whole tree:
			var findAndDeleteCommentInTree = function(parent) {
				if (parent.comments) {
					for (var i in parent.comments) {
						var comment = parent.comments[i];
						if (comment.id === commentId) {
							parent.comments.splice(i, 1);
							return true;
						}
						if (findAndDeleteCommentInTree(comment)) {
							return true;
						}
					}
				}
				return false;
			};
			findAndDeleteCommentInTree($scope.giftBag);
		});
	};
	
	$scope.isCommentDeletable = function(comment) {
		return $scope.userAccount.userId === comment.owner && (!comment.comments || comment.comments.length === 0);
	};
	
	$scope.isCommentEditable = function(comment) {
		return $scope.userAccount.userId === comment.owner;
	};

	$scope.resetReply = function() {
		$scope.newComment = {};
		$scope.replyingToComment = null;
	};

	$scope.openReplyToComment = function(parent) {
		$scope.newComment.parentId = parent.id;
		$scope.replyingToComment = parent.id;
	};
	
	$scope.openEditComment = function(comment) {
		$scope.newComment = comment;
		$scope.replyingToComment = comment.id;
	};
	
});
