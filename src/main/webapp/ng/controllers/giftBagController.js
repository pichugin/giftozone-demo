angular.module('giftozone.controllers').controller("GiftBagCtrl", function($scope, $routeParams, $filter, $modal, $location, 
		GiftBagService, OccasionService, UserService, InvitationService, Utils) {
	
	$scope.pageState = {}; //state for in-place edit
	var oldBudget;
	$scope.invitees = []; //List of invitees to this bag.
	$scope.newInvitee = {};
	
	GiftBagService.getGiftBag($routeParams.occasionId, $routeParams.giftBagId, function(bag) {
		
		// TODO: This happens when a non-owner opens a gift bag:
		if (!bag) {
			alert("Error! Gift bag is undefined!");
			return;
		}
		
		$scope.giftBag = bag;
		$scope.maxVotes = calculateMaxVotes(bag);
		getGiftBagInvitees(bag);
		UserService.addRecentGiftBag(bag);
	});
	
	function calculateMaxVotes(giftBag) {
		var maxVotes = 0;
		if (giftBag.giftProposals) {
			for (var i=0; i<giftBag.giftProposals.length; i++) {
				if (giftBag.giftProposals[i].voters && maxVotes < giftBag.giftProposals[i].voters.length) {
					maxVotes = giftBag.giftProposals[i].voters.length;
				}
			}
		}
		return maxVotes;
	}

	function getGiftBagInvitees(giftBag) {
		InvitationService.getGiftBagInvitees(giftBag.occasionId, giftBag.id, function(invitees) {
			$scope.invitees = invitees;
		});
	}
	
	OccasionService.getOccasion($routeParams.occasionId, function(occ) {
		$scope.occasion = occ;
	});
	
	UserService.getCurrentUser(function(user) {
		$scope.userAccount = user;
		
		UserService.getUserContacts(user.userId, function(contacts) {
			$scope.userContacts = contacts;
		});
	});

	InvitationService.getInvitations(function(invitations) {
		$scope.invitationList = invitations;
	});

	//TODO: Copied from occasionController.js, move it to InvitationService.js:
	$scope.getGiftBagInvitation = function(occasionId, giftBagId) {
		for (var i=0; i < $scope.invitationList.length; i++) {
			var invitation = $scope.invitationList[i];
			if (invitation.occasionId == occasionId && invitation.giftBagId == giftBagId) {
				return invitation;
			}
		}
		return null;
	};
	
	//TODO: Copied from occasionController.js, move it to InvitationService.js:
	$scope.acceptGiftBagInvitation = function(invitationId) {
		InvitationService.acceptInvitationToGiftBag(invitationId, function() {
			$scope.invitation = null;
		});
	}


	// TODO: Insert to bottom, not after current bag:
	$scope.createGiftBag = function(occasionId) {
		GiftBagService.updateGiftBag({"occasionId" : occasionId}, function(giftBag) {
			$location.path("/occasion/" + giftBag.occasionId + "/giftbag/" + giftBag.id);
		});
	};
	
	$scope.deleteGiftBagConfirm = function(occasionId, giftBagId) {
		openConfirmModal("Are you sure you want to delete this gift bag?", function() {
			GiftBagService.deleteGiftBag(occasionId, giftBagId, function() {
				// Remove giftBag from the model after it was successfully deleted on the server:
				$scope.occasion.giftBags.splice(Utils.getIndexById($scope.occasion.giftBags, giftBagId), 1);
				$scope.giftBag = $scope.occasion.giftBags[0];
			});
		});
	};

	function updateScopeGiftProposal(giftProposal) {
		for (var i=0; i<$scope.giftBag.giftProposals.length; i++) {
			if ($scope.giftBag.giftProposals[i].id == giftProposal.id) {
				$scope.giftBag.giftProposals[i] = giftProposal;
			}
		}
	}
	
	function getGiftProposalById(proposalId) {
		for (var i=0; i<$scope.giftBag.giftProposals.length; i++) {
			if ($scope.giftBag.giftProposals[i].id == proposalId) {
				return $scope.giftBag.giftProposals[i];
			}
		}
		return null;
	}
	
	function deleteGiftProposal(proposalId) {
		for (var i=0; i<$scope.giftBag.giftProposals.length; i++) {
			if ($scope.giftBag.giftProposals[i].id == proposalId) {
				$scope.giftBag.giftProposals.splice(i, 1);
				return;
			}
		}
	}
	
	$scope.editGiftBudget = function() {
		oldBudget = angular.copy($scope.giftBag.budget);
		$scope.pageState.editBudget = true;
	};
	
	//This is needed to bind to an input of type number
	$scope.$watch('giftBag.budget', function(val, old) {
		if ($scope.giftBag) {
			$scope.giftBag.budget = parseFloat(val); 
		}
	});

	$scope.saveEditGiftBudget = function() {
		oldBudget = null;
		$scope.pageState.editBudget = false;
		GiftBagService.updateGiftBag($scope.giftBag, function(savedGiftBag) {
			$scope.giftBag = savedGiftBag;
		});
	};
	
	$scope.cancelEditGiftBudget = function() {
		$scope.giftBag.budget = oldBudget;
		$scope.pageState.editBudget = false;
	};
	
	$scope.inviteParticipants = function() {
		InvitationService.inviteGiftBagParticipant($scope.giftBag.occasionId, $scope.giftBag.id, 
				$scope.newInvitee.email, $scope.newInvitee.suggestCurrentGiftBag, function(savedInvitation) {
			$scope.invitees.push($scope.newInvitee);
			
			//TODO: ??
			//alert(JSON.stringify(savedInvitation));
			
			$scope.cancelInviteParticipants();
		});
	};
	
	$scope.cancelInviteParticipants = function() {
		$scope.newInvitee = {};
		$scope.pageState.inviteParticipants = false;
	}
	
	$scope.openCreateGiftProposalModal = function() {
		openEditGiftProposalModal({}, function(newGiftProposal) {
			GiftBagService.updateGiftProposal($scope.giftBag.occasionId, $scope.giftBag.id, newGiftProposal, function(savedProposal) {
				if (!$scope.giftBag.giftProposals) {
					$scope.giftBag.giftProposals = [];
				}
				$scope.giftBag.giftProposals.push(savedProposal);
			});
		});
	};
	
	$scope.openEditGiftProposalModal = function(proposalId) {
		var proposalToEdit = getGiftProposalById(proposalId);
		openEditGiftProposalModal({id : proposalToEdit.id, name : proposalToEdit.name}, function(updatedGiftProposal) {
			proposalToEdit.name = updatedGiftProposal.name;
			GiftBagService.updateGiftProposal($scope.giftBag.occasionId, $scope.giftBag.id, proposalToEdit, function(savedProposal) {
				updateScopeGiftProposal(savedProposal);
			});
		});
	};
	
	$scope.deleteGiftProposalConfirm = function(proposalId) {
		openConfirmModal("Are you sure you want to delete this gift idea?", function() {
			GiftBagService.deleteGiftProposal($scope.giftBag.occasionId, $scope.giftBag.id, proposalId, function() {
				deleteGiftProposal(proposalId);
			});
		});
	};
	
	$scope.voteForGiftProposal = function(proposalId) {
		GiftBagService.voteForGiftProposal($scope.giftBag.occasionId, $scope.giftBag.id, proposalId, function(giftProposal) {
			updateScopeGiftProposal(giftProposal);
			$scope.maxVotes = calculateMaxVotes($scope.giftBag);
		});
	};
	
	$scope.unvoteForGiftProposal = function(proposalId) {
		GiftBagService.unvoteForGiftProposal($scope.giftBag.occasionId, $scope.giftBag.id, proposalId, $scope.userAccount.userId, function(giftProposal) {
			updateScopeGiftProposal(giftProposal);
			$scope.maxVotes = calculateMaxVotes($scope.giftBag);
		});
	};
	
	function openEditGiftProposalModal(proposal, cb) {
		var modalInstance = $modal.open({
			templateUrl: 'partials/giftBag/editGiftProposalModal.html',
			controller: EditGiftProposalModalCtrl,
			resolve: {
				giftProposal : function() {
					return proposal;
				}
			}
		});
	
		modalInstance.result.then(function(proposal) {
			cb(proposal);
		});
	}
	
	//TODO: Move outside of this controller
	$scope.inArray = function(item, array) {
		for (var i=0; i<array.length; i++) {
			if (array[i] == item) {
				return true;
			}
		}
		return false;
	};
	
	//TODO: Consider taking out to be reused by other controllers
	function openConfirmModal(msg, cb) {
		var modalInstance = $modal.open({
			templateUrl: 'partials/confirmModal.html',
			controller: ConfirmModalCtrl,
			resolve: {
				message : function() {
					return msg;
				}
			}
		});
	
		modalInstance.result.then(cb);
	}
	
});


//TODO: figure out how to create this within the giftozone controllers module, not in the global scope:
var EditGiftProposalModalCtrl = function($scope, $modalInstance, giftProposal) {
	$scope.giftProposal = giftProposal;
	
	$scope.ok = function() {
		$modalInstance.close($scope.giftProposal);
	};
	
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
};