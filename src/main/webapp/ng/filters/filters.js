angular.module('giftozone.filters').filter('gzUserIdToName', function() {
	return function(userId, userContacts) {
		if (userId != undefined && userContacts != undefined) {
			for (var i=0; i<userContacts.length; i++) {
				if (userContacts[i].userId == userId) {
					return userContacts[i].displayName ? userContacts[i].displayName : userId;
				}
			}
		}
		return userId ? userId : "";
	};
});

angular.module('giftozone.filters').filter('gzUserIdToEmail', function() {
	return function(userId, userContacts) {
		if (userId != undefined && userContacts != undefined) {
			for (var i=0; i<userContacts.length; i++) {
				if (userContacts[i].userId == userId) {
					return userContacts[i].email ? userContacts[i].email : "";
				}
			}
		}
		return "";
	}
});

//In: List of user ids; Out: comma-separated user names;
angular.module('giftozone.filters').filter('gzConcatUsers', function($filter) {
	var userIdToNameFilter = $filter("gzUserIdToName");
	return function(userIds, userContacts) {
		var result = "";
		for (var i=0; i<userIds.length; i++) {
			result += userIdToNameFilter(userIds[i], userContacts) + ((i < userIds.length - 1) ? ", " : "");
		}
		return result;
	};
});

//In: List of gift proposal objects; Out: comma-separated gift proposal names;
angular.module('giftozone.filters').filter('gzConcatGiftProposals', function($filter) {
	return function(giftProposals) {
		var result = "";
		if (giftProposals) {
			for (var i=0; i<giftProposals.length; i++) {
				result += giftProposals[i].name + ((i < giftProposals.length - 1) ? ", " : "");
			}
		}
		return result;
	};
});

//TODO: Combine the following 3. Only difference is the format.
angular.module('giftozone.filters').filter('gzDate', function($filter) {
	var angularDateFilter = $filter('date');
	return function(theDate) {
		return theDate ? angularDateFilter(theDate, 'MMM d, yyyy') : '';
	};
});

angular.module('giftozone.filters').filter('gzMonthAndDay', function($filter) {
	var angularDateFilter = $filter('date');
	return function(theDate) {
		return theDate ? angularDateFilter(theDate, 'MMM d') : '';
	};
});

angular.module('giftozone.filters').filter('gzYear', function($filter) {
	var angularDateFilter = $filter('date');
	return function(theDate) {
		return theDate ? angularDateFilter(theDate, 'yyyy') : '';
	};
});

angular.module('giftozone.filters').filter('gzFromNow', function() {
	return function(date) {
		return (date) ? moment(date).fromNow() : "";
	};
});

angular.module('giftozone.filters').filter('gzNumToDollars', function() {
	return function(num) {
		if (!num) {
			return "$0.00";
		} else if (num < 0) {
			return "-$" + Math.abs(num).toFixed(2);
		}
		return "$" + num.toFixed(2);
	};
});