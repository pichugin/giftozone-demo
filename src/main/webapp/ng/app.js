angular.module('giftozone.services', []);
angular.module('giftozone.controllers', []);
angular.module('giftozone.filters', []);
angular.module('giftozone.directives', []);

var giftozone = angular.module('giftozone', ['ngRoute', 'ui.bootstrap', 'giftozone.services', 'giftozone.controllers', 'giftozone.filters', 'giftozone.directives']);

giftozone.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/occasions', {templateUrl: 'partials/occasions.html', controller: 'OccasionListCtrl'});
	$routeProvider.when('/occasion/:occasionId/giftbag/:giftBagId', {templateUrl: 'partials/giftBag.html', controller: 'GiftBagCtrl'});
	$routeProvider.when('/user/:userId', {templateUrl: 'partials/userProfile.html', controller: 'UserProfileCtrl'});
	$routeProvider.when('/about', {templateUrl: 'partials/about.html', controller: 'AboutCtrl'});
	$routeProvider.otherwise({redirectTo: '/occasions'});
}]);

//Change default configuration of Angular-UI datepicker
giftozone.config(['datepickerConfig', function(datepickerConfig) {
	//alert(JSON.stringify(datepickerConfig));
	datepickerConfig.showWeeks = false;
}]);
