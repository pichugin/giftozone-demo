angular.module('giftozone.directives').directive('gzAffix', function() {
	var topOffset = 80; //TODO: Parameterize
	var elementAffixed = false;
	
	function link(scope, element, attrs) {
		$(window).on('scroll', function() {
			var scrollTop = $(window).scrollTop();
			if (scrollTop > topOffset && !elementAffixed) {
				affixElement(element);
				elementAffixed = true;
			} else if (scrollTop <= topOffset && elementAffixed) {
				unAffixElement(element);
				elementAffixed = false;
			}
		});
	}
	
	function affixElement(element) {
		element.css('top', element.offset().top - topOffset);
		element.css('width', element.outerWidth());
		element.css('position', 'fixed');
	}
	
	function unAffixElement(element) {
		element.css('top', '');
		element.css('width', '');
		element.css('position', 'relative');
	}
	
	return {
		restrict : 'A',
		link : link
	}
})

angular.module('giftozone.directives').directive('gzAutofocus', function($timeout) {
	function link(scope, element, attrs) {
		$timeout(function() {
			element.focus();
		}, 100);
	}
	
	return {
		restrict : 'A',
		link : link
	};
});

angular.module('giftozone.directives').directive('gzUserLink', function($timeout) {
	return {
		restrict : 'E',
		scope : {
			linkUserId : '=userId',
			linkUserContacts : '=userContacts'
		},
		controller: function($scope) {
			$scope.hoverUserId;
			var mouseoverTimeout;
			
			$scope.mouseover = function(userId) {
				mouseoverTimeout = $timeout(function() {
					$scope.hoverUserId = userId;
				}, 1000);
			}
			
			$scope.mouseleave = function() {
				$timeout.cancel(mouseoverTimeout);
				$scope.hoverUserId = null;
			}
		},
		templateUrl : 'partials/directives/userLink.html'
	};
});

angular.module('giftozone.directives').directive('gzFileUpload', function () {
    var link = function ($scope, element, attributes) {
        element.bind('change', function (event) {
            var files = event.target.files;
            $scope.$apply(function () {
                for (var i = 0, length = files.length; i < length; i++) {
                	$scope.file = files[i];
                }
            });
        });
    };
    return {
        restrict: 'A',
        link: link,
        scope: {
        	fieldName: "@gzFileUpload",
        	maxSize: "@maxSize",
        	formats: "@formats",
        	handler: "=handler"
        },
        controller: function($scope) {
        	$scope.$watch('file', function (newValue, oldValue) {
        		if ($scope.file) {
        			$scope.handler($scope.file, $scope.fieldName, $scope.maxSize, $scope.formats);
        		}
        	}, true);
        }
    };
});
