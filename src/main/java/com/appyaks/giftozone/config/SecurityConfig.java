package com.appyaks.giftozone.config;

import java.util.Arrays;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.RememberMeAuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.security.SocialAuthenticationFilter;
import org.springframework.social.security.SocialAuthenticationProvider;
import org.springframework.social.security.SocialAuthenticationServiceLocator;

import com.appyaks.giftozone.domain.account.MongoPersistentTokenRepositoryImpl;
import com.appyaks.giftozone.domain.account.RememberMeTokenRepository;
import com.appyaks.giftozone.domain.account.UserService;
import com.appyaks.giftozone.security.CustomUserDetailsService;
import com.appyaks.giftozone.security.SecurityLoginFailureHandler;
import com.appyaks.giftozone.security.SecurityLoginSuccessHandler;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Inject
    private Environment environment;

    @Inject 
    private UserService userService;
    
    @Inject
    private RememberMeTokenRepository rememberMeTokenRepository;
    
    @Inject
    private UsersConnectionRepository usersConnectionRepository;
    
    @Inject
    private SocialAuthenticationServiceLocator socialAuthenticationServiceLocator;
    
    @Inject
    private CustomUserDetailsService customUserDetailsService;
    
    @Bean
    public SocialAuthenticationFilter socialAuthenticationFilter() throws Exception {
        SocialAuthenticationFilter filter = new SocialAuthenticationFilter(authenticationManager(), userService,
                usersConnectionRepository, socialAuthenticationServiceLocator);
        filter.setFilterProcessesUrl("/signin");
        filter.setSignupUrl("/signup"); 
        filter.setConnectionAddedRedirectUrl("/");
        filter.setPostLoginUrl("/"); //always open account profile page after login
        filter.setRememberMeServices(rememberMeServices());
        return filter;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
    	return new StandardPasswordEncoder();
    }
    
    @Bean
    public UsernamePasswordAuthenticationFilter usernamePasswordAuthenticationFilter() throws Exception {
    	UsernamePasswordAuthenticationFilter filter = new UsernamePasswordAuthenticationFilter();
    	
    	DaoAuthenticationProvider daoAuthProvider = new DaoAuthenticationProvider();
    	daoAuthProvider.setUserDetailsService(customUserDetailsService);
//    	ReflectionSaltSource saltSource = new ReflectionSaltSource();
//    	saltSource.setUserPropertyToUse("email");
//    	daoAuthProvider.setSaltSource(saltSource);
    	daoAuthProvider.setPasswordEncoder(passwordEncoder());
    	AuthenticationManager authManager = new ProviderManager(Arrays.asList(new AuthenticationProvider[] {daoAuthProvider}));

    	filter.setAuthenticationManager(/*authenticationManager()*/authManager);
    	filter.setFilterProcessesUrl("/j_spring_security_check");
    	filter.setAuthenticationSuccessHandler(securityLoginSuccessHandler());
    	filter.setAuthenticationFailureHandler(securityLoginFailureHandler());
    	return filter;
    }

    @Bean
    public SocialAuthenticationProvider socialAuthenticationProvider(){
        return new SocialAuthenticationProvider(usersConnectionRepository, userService);
    }
    
//    @Bean
//    public LoginUrlAuthenticationEntryPoint socialAuthenticationEntryPoint() {
//        return new LoginUrlAuthenticationEntryPoint("/signin");
//    }

    @Bean
    public RememberMeServices rememberMeServices(){
        PersistentTokenBasedRememberMeServices rememberMeServices = new PersistentTokenBasedRememberMeServices(
                        environment.getProperty("application.key"), userService, persistentTokenRepository());
        rememberMeServices.setAlwaysRemember(true);
        return rememberMeServices;
    }
    
    @Bean 
    public RememberMeAuthenticationProvider rememberMeAuthenticationProvider(){
        RememberMeAuthenticationProvider rememberMeAuthenticationProvider = 
                        new RememberMeAuthenticationProvider(environment.getProperty("application.key"));
        return rememberMeAuthenticationProvider; 
    }

    @Bean 
    public PersistentTokenRepository persistentTokenRepository() {
        return new MongoPersistentTokenRepositoryImpl(rememberMeTokenRepository);
    }

    @Override
    public void configure(WebSecurity builder) throws Exception {
        builder.ignoring().antMatchers("/resources/**");
    }

	@Override
	protected void configure(HttpSecurity http) throws Exception {
        http
	        //.authorizeRequests() 
	        .authorizeUrls()
	            .antMatchers("/favicon.ico", "robots.txt", "/site/**").permitAll()
	            .antMatchers("/acceptinvitation/**", "/confirmRegistration").permitAll()
	            .antMatchers("/app/**", "/api/**", "/occasions", "/giftBag").authenticated()
	            .and()
	        .addFilterBefore(socialAuthenticationFilter(), AbstractPreAuthenticatedProcessingFilter.class)
	        .addFilterBefore(usernamePasswordAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
	        .logout()
	            .deleteCookies("JSESSIONID")
	            .logoutUrl("/signout")
	            .logoutSuccessUrl("/")
	            .permitAll()
	            .and()
	        .rememberMe()
	            .rememberMeServices(rememberMeServices());
	}
	
	private SecurityLoginSuccessHandler securityLoginSuccessHandler() {
		SecurityLoginSuccessHandler securityLoginSuccessHandler = new SecurityLoginSuccessHandler();
		// This is for non-AJAX requests:
		securityLoginSuccessHandler.setDefaultTargetUrl("/"); 
		return securityLoginSuccessHandler;
	}
	
	private SecurityLoginFailureHandler securityLoginFailureHandler() {
		SecurityLoginFailureHandler securityLoginFailureHandler = new SecurityLoginFailureHandler();
		// This is for non-AJAX requests:
		securityLoginFailureHandler.setDefaultFailureUrl("/loginfailed");
		return securityLoginFailureHandler;
	}
    
    @Override
    protected void registerAuthentication(AuthenticationManagerBuilder builder) throws Exception {
        builder
            .authenticationProvider(socialAuthenticationProvider())
            .authenticationProvider(rememberMeAuthenticationProvider())
            .userDetailsService(userService);
    }
    
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
    
}
