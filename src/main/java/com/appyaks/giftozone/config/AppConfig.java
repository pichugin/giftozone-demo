package com.appyaks.giftozone.config;

import java.io.IOException;
import java.util.TimeZone;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.ui.velocity.VelocityEngineFactory;

import com.appyaks.giftozone.domain.CommentService;
import com.appyaks.giftozone.domain.CommentServiceImpl;
import com.appyaks.giftozone.domain.FeedbackService;
import com.appyaks.giftozone.domain.FeedbackServiceImpl;
import com.appyaks.giftozone.domain.GiftBagService;
import com.appyaks.giftozone.domain.GiftBagServiceImpl;
import com.appyaks.giftozone.domain.InvitationService;
import com.appyaks.giftozone.domain.InvitationServiceImpl;
import com.appyaks.giftozone.domain.MailService;
import com.appyaks.giftozone.domain.MailServiceImpl;
import com.appyaks.giftozone.domain.OccasionService;
import com.appyaks.giftozone.domain.OccasionServiceImpl;
import com.appyaks.giftozone.domain.SynchService;
import com.appyaks.giftozone.domain.SynchServiceImpl;
import com.appyaks.giftozone.domain.account.CounterService;
import com.appyaks.giftozone.domain.account.CounterServiceImpl;
import com.appyaks.giftozone.domain.account.UserRepository;
import com.appyaks.giftozone.domain.account.UserService;
import com.appyaks.giftozone.domain.account.UserServiceImpl;
import com.appyaks.giftozone.domain.account.UserSocialConnectionRepository;
import com.appyaks.giftozone.security.CustomUserDetailsService;


@Configuration
public class AppConfig {

	//TODO: Find a better place to initialize the default time zone:
	static {
		// Without this all dates get screwed:
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}
	
	@Bean 
	public CustomUserDetailsService customUserDetailsService() {
		return new CustomUserDetailsService();
	}
	
	@Bean
	public CommentService commentService() {
		return new CommentServiceImpl();
	}

	@Bean
	public OccasionService occasionService() {
		return new OccasionServiceImpl();
	}
	
	@Bean
	public GiftBagService giftBagService() {
		return new GiftBagServiceImpl();
	}
	
	@Bean
	public InvitationService invitationService() {
		return new InvitationServiceImpl();
	}
	
	@Bean
	public SynchService synchService() {
		return new SynchServiceImpl();
	}

    @Bean
    public UserService accountService(MongoTemplate mongoTemplate, UserRepository accountRepository,
    		UserSocialConnectionRepository userSocialConnectionRepository, MailService mailService) {
        return new UserServiceImpl(accountRepository, userSocialConnectionRepository, counterService(mongoTemplate), mailService);
    }

    @Bean
    public CounterService counterService(MongoTemplate mongoTemplate) {
        return new CounterServiceImpl(mongoTemplate);
    }

    @Bean 
    public MailService mailService() {
    	return new MailServiceImpl();
    }
    
    @Bean
    public VelocityEngine velocityEngine() throws VelocityException, IOException {
    	VelocityEngineFactory factory = new VelocityEngineFactory();
    	factory.setResourceLoaderPath("classpath:/velocity");
    	return factory.createVelocityEngine();
    }

}
