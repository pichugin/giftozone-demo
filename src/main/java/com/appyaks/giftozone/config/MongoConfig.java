package com.appyaks.giftozone.config;

import javax.inject.Inject;

import org.jboss.logging.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

@Configuration
@EnableMongoRepositories(basePackages = "com.appyaks.giftozone.domain")
public class MongoConfig extends AbstractMongoConfiguration {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
    @Inject
    private Environment environment;

	@Override
	public @Bean Mongo mongo() throws Exception {
//		String mongoHost = System.getenv("OPENSHIFT_MONGODB_DB_HOST");
//		String mongoPort = System.getenv("OPENSHIFT_MONGODB_DB_PORT");
//		return new MongoClient(
//				mongoHost != null ? mongoHost : "localhost",
//				mongoPort != null ? Integer.parseInt(mongoPort) : 27017);
		
		logger.info("Using profile: " + environment.getProperty("profile.info"));
		
		String mongoUrl = System.getenv("OPENSHIFT_MONGODB_DB_URL");
		return new MongoClient(new MongoClientURI(mongoUrl != null ? mongoUrl : environment.getProperty("mongodb.url")));
	}
 
	@Override
	public @Bean MongoTemplate mongoTemplate() throws Exception {
		return new MongoTemplate(mongo(), getDatabaseName());
	}

	@Override
	public String getDatabaseName() {
        return environment.getProperty("mongodb.dbname");
	}
//
//    @Override
//    protected UserCredentials getUserCredentials() {
//    	String username = System.getenv("OPENSHIFT_MONGODB_DB_USERNAME");
//    	if (username == null) {
//    		username = environment.getProperty("mongodb.username");
//    	}
//    	String password = System.getenv("OPENSHIFT_MONGODB_DB_PASSWORD");
//    	if (password == null) {
//    		password = environment.getProperty("mongodb.password");
//    	}
//        return new UserCredentials(username, password);
//    }

    @Bean
	public GridFsTemplate gridFsTemplate() throws Exception {
		return new GridFsTemplate(mongoDbFactory(), mappingMongoConverter());
	}

}
