package com.appyaks.giftozone.config;

import java.util.Properties;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class MailConfig {

    @Inject
    private Environment environment;

    @Bean
    public TaskExecutor mailTaskExecutor() {
    	ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
    	taskExecutor.setCorePoolSize(3);
    	taskExecutor.setMaxPoolSize(10);
    	taskExecutor.setQueueCapacity(30);
    	return taskExecutor;
    } 
    
	public @Bean JavaMailSender mailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

		Properties mailProps = new Properties();
		mailProps.put("mail.smtp.auth", environment.getProperty("mail.smtp.auth")); 
		mailProps.put("mail.smtp.starttls.enable", environment.getProperty("mail.smtp.starttls.enable"));
		mailSender.setJavaMailProperties(mailProps);

		mailSender.setProtocol(environment.getProperty("mail.protocol")); 
		mailSender.setPort(Integer.parseInt(environment.getProperty("mail.port")));	
		mailSender.setHost(environment.getProperty("mail.host"));
		mailSender.setUsername(environment.getProperty("mail.username"));
		mailSender.setPassword(environment.getProperty("mail.password"));
		return mailSender;
	}

//	public @Bean SimpleMailMessage mailMessage() {
//		SimpleMailMessage mailMessage = new SimpleMailMessage();
//		mailMessage.setFrom(environment.getProperty("mail.username"));
//		return mailMessage;
//	}
	
}
