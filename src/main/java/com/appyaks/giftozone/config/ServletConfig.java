package com.appyaks.giftozone.config;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;
import org.springframework.web.servlet.view.xml.MarshallingView;


@Configuration
@ComponentScan(basePackages = { "com.appyaks.giftozone.web.controller" })
@EnableWebMvc
public class ServletConfig extends WebMvcConfigurerAdapter {

	public static final String DATE_FORMAT_INCOMING = "yyyy-MM-dd";  //"yyyy-MM-dd HH:mm:ss.S";
	public static final String DATE_FORMAT_OUTGOING = "yyyy-MM-dd";  //"yyyy-MM-dd HH:mm:ss.S";
	private static final String MESSAGE_SOURCE_BASE_NAME = "i18n/messages";
	

	@Bean
    public MultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(500000);
        return multipartResolver;
    }
	
	@Bean
	public LocalValidatorFactoryBean getValidator() {

		LocalValidatorFactoryBean lvfb = new LocalValidatorFactoryBean();
		//lvfb.setValidationMessageSource(getValidationMessageSource());
		return lvfb;
	}

	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		MappingJackson2HttpMessageConverter jackson2httpConverter = new MappingJackson2HttpMessageConverter();
		jackson2httpConverter.getObjectMapper().setDateFormat(new SimpleDateFormat(DATE_FORMAT_INCOMING));
//		jackson2httpConverter.getObjectMapper().setTimeZone(TimeZone.getTimeZone("GMT"));
		converters.add(jackson2httpConverter);
		super.configureMessageConverters(converters);
	}

	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers.add(new PageableHandlerMethodArgumentResolver());
	}
	
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/styles/*.css").addResourceLocations("/", "/styles/").setCachePeriod(1);
		registry.addResourceHandler("/images/**").addResourceLocations("/", "/images/").setCachePeriod(1);
		registry.addResourceHandler("/script/*.js").addResourceLocations("/", "/script/").setCachePeriod(1);
	}

	public @Bean HandlerExceptionResolver exceptionResolver() {
		Properties mappings = new Properties();
		SimpleMappingExceptionResolver resolver = new SimpleMappingExceptionResolver();
		resolver.setExceptionMappings(mappings);
		return resolver;
	}

	public @Bean ContentNegotiatingViewResolver contentNegotiatingViewResolver() {
		ContentNegotiatingViewResolver resolver = new ContentNegotiatingViewResolver();
		Map<String, String> mediaTypes = new HashMap<String, String>();
		mediaTypes.put("json", "application/json");
		resolver.setMediaTypes(mediaTypes);
		
		MappingJacksonJsonView jsonView = new MappingJacksonJsonView();
		// Configure date rendering (parsing is configured in configureMessageConverters()):
		jsonView.getObjectMapper().setDateFormat(new SimpleDateFormat(DATE_FORMAT_OUTGOING));// HH:mm:ss.S"));
		jsonView.getObjectMapper().setSerializationInclusion(Inclusion.NON_NULL); //This is to avoid nulls in the rendered object
		
		resolver.setDefaultViews(Arrays.asList(new View[] { 
				new MarshallingView(), 
				jsonView 
		}));
		resolver.setViewResolvers(Arrays.asList(new ViewResolver[] { viewResolver() } ));
		return resolver;
	}

	/**
	 * This bean configures the 'prefix' and 'suffix' properties of
	 * InternalResourceViewResolver, * which resolves logical view
	 * names returned by Controllers. For example, a logical view name
	 * of "vets" will be mapped to "/WEB-INF/jsp/vets.jsp".
	 */
	public @Bean ViewResolver viewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setViewClass(JstlView.class);
		resolver.setPrefix("/WEB-INF/jsp/");
		resolver.setSuffix(".jsp");
		return resolver;
	}

	public @Bean MessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename(MESSAGE_SOURCE_BASE_NAME);
		messageSource.setUseCodeAsDefaultMessage(true);
		return messageSource;
	}

}
