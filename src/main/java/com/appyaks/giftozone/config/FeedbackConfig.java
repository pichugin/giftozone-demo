package com.appyaks.giftozone.config;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.appyaks.giftozone.domain.FeedbackService;
import com.appyaks.giftozone.domain.FeedbackServiceImpl;

@Configuration
public class FeedbackConfig {

	@Inject
	private Environment environment;

	@Bean
	public FeedbackService feedbackService() {
		return new FeedbackServiceImpl(environment.getProperty("feedback.email"));
	}

}
