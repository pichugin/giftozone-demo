package com.appyaks.giftozone.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Profile("openshift")
@PropertySource(value={"classpath:cloudConfig.properties"})
public class CloudConfig {

}