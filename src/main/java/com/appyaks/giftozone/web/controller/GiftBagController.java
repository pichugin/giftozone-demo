package com.appyaks.giftozone.web.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.appyaks.giftozone.domain.GiftBag;
import com.appyaks.giftozone.domain.GiftBagService;
import com.appyaks.giftozone.domain.GiftProposal;
import com.appyaks.giftozone.web.SessionUtils;

@Controller
public class GiftBagController {

	@Inject
	private GiftBagService giftBagService;
	
	
	@RequestMapping(value = "/api/occasions/{occasionId}/giftBags", method = RequestMethod.GET)
	public List<GiftBag> getGiftBags(
			@PathVariable(value = "occasionId") String occasionId) {
		return giftBagService.getGiftBags(SessionUtils.getUserId(), occasionId);
	}

	@RequestMapping(value = "/api/occasions/{occasionId}/giftBags/{giftBagId}", method = RequestMethod.GET, headers="Accept=application/json")
	public GiftBag getGiftBag(
			@PathVariable(value = "occasionId") String occasionId,
			@PathVariable(value = "giftBagId") String giftBagId) {
		return giftBagService.getGiftBag(giftBagId, occasionId, SessionUtils.getUserId());
	}

	@RequestMapping(value = "/api/occasions/{occasionId}/giftBags", method = RequestMethod.POST)
	public GiftBag createGiftBag(
			@PathVariable(value = "occasionId") String occasionId, 
			@RequestBody final GiftBag giftBag) {
		return giftBagService.saveGiftBag(giftBag, occasionId, SessionUtils.getUserId());
	}

	@RequestMapping(value = "/api/occasions/{occasionId}/giftBags/{giftBagId}", method = RequestMethod.POST)
	public GiftBag updateGiftBag(
			@PathVariable(value = "occasionId") String occasionId,
			@PathVariable(value = "giftBagId") String giftBagId,
			@RequestBody final GiftBag giftBag) {
		giftBag.setId(giftBagId);
		return giftBagService.saveGiftBag(giftBag, occasionId, SessionUtils.getUserId());
	}

	@RequestMapping(value = "/api/occasions/{occasionId}/giftBags/{giftBagId}", 
			method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public void deleteGiftBag(
			@PathVariable(value="occasionId") String occasionId,
			@PathVariable(value="giftBagId") String giftBagId) {
		/*
		 * TODO: OccasionId is not used. Is it necessary?
		 */
		giftBagService.deleteGiftBag(giftBagId, SessionUtils.getUserId());
	}
	
	@RequestMapping(value = "/api/occasions/{occasionId}/giftBags/{giftBagId}/participants/{participantId}", 
			method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public void inviteParticipant(
			@PathVariable(value="occasionId") String occasionId,
			@PathVariable(value="giftBagId") String giftBagId,
			@PathVariable(value="participantId") String participantId) {
		
		//TODO: Figure out the logic. This is the case when we invite an existing user into the giftBag. Here we need to
		//      ensure the invitee is in the occasion participants, and then send him an email (the same email we send through
		//      InvitationService.inviteByEmail()) or somehow contact the invitee through available social network connections. 
		giftBagService.addParticipant(participantId, giftBagId, occasionId, SessionUtils.getUserId());
	}

	@RequestMapping(value = "/api/occasions/{occasionId}/giftBags/{giftBagId}/participants/{participantId}", 
			method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public void deleteParticipant(
			@PathVariable(value="occasionId") String occasionId,
			@PathVariable(value="giftBagId") String giftBagId,
			@PathVariable(value="participantId") String participantId) {
		giftBagService.deleteParticipant(participantId, giftBagId, occasionId, SessionUtils.getUserId());
	}
	
	@RequestMapping(value = "/api/occasions/{occasionId}/giftBags/{giftBagId}/giftProposals", 
			method = RequestMethod.POST)
	public GiftProposal createGiftProposal(
			@PathVariable(value="occasionId") String occasionId,
			@PathVariable(value="giftBagId") String giftBagId,
			@RequestBody final GiftProposal giftProposal) {
		return giftBagService.saveGiftProposal(giftProposal, giftBagId, occasionId, SessionUtils.getUserId());
	}

	@RequestMapping(value = "/api/occasions/{occasionId}/giftBags/{giftBagId}/giftProposals/{giftProposalId}", 
			method = RequestMethod.POST)
	public GiftProposal updateGiftProposal(
			@PathVariable(value="occasionId") String occasionId,
			@PathVariable(value="giftBagId") String giftBagId,
			@PathVariable(value="giftProposalId") String giftProposalId,
			@RequestBody final GiftProposal giftProposal) {
		giftProposal.setId(giftProposalId);
		return giftBagService.saveGiftProposal(giftProposal, giftBagId, occasionId, SessionUtils.getUserId());
	}

	@RequestMapping(value = "/api/occasions/{occasionId}/giftBags/{giftBagId}/giftProposals/{giftProposalId}", 
			method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public void deleteGiftProposal(
			@PathVariable(value="occasionId") String occasionId,
			@PathVariable(value="giftBagId") String giftBagId,
			@PathVariable(value="giftProposalId") String giftProposalId) {
		giftBagService.deleteGiftProposal(giftProposalId, giftBagId, occasionId, SessionUtils.getUserId());
	}

	@RequestMapping(value = "/api/occasions/{occasionId}/giftBags/{giftBagId}/giftProposals/{giftProposalId}/voters", 
			method = RequestMethod.POST)
	//TODO: Figure out how to return an Integer instead of the whole object
	public GiftProposal voteForGiftProposal(
			@PathVariable(value="occasionId") String occasionId,
			@PathVariable(value="giftBagId") String giftBagId,
			@PathVariable(value="giftProposalId") String giftProposalId) {
		return giftBagService.voteForProposal(giftProposalId, giftBagId, occasionId, SessionUtils.getUserId());
	}

	@RequestMapping(value = "/api/occasions/{occasionId}/giftBags/{giftBagId}/giftProposals/{giftProposalId}/voters/{userId}", 
			method = RequestMethod.DELETE)
	//TODO: Figure out how to return an Integer instead of the whole object
	public GiftProposal unvoteForGiftProposal(
			@PathVariable(value="occasionId") String occasionId,
			@PathVariable(value="giftBagId") String giftBagId,
			@PathVariable(value="giftProposalId") String giftProposalId,
			@PathVariable(value="userId") String userId) {
		String uid = SessionUtils.getUserId();
		Assert.isTrue(uid.equals(userId), "You cannot vote on behalf of another user");
		return giftBagService.unvoteForProposal(giftProposalId, giftBagId, occasionId, uid);
	}

}
