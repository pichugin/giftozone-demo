package com.appyaks.giftozone.web.controller;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.appyaks.giftozone.domain.Feedback;
import com.appyaks.giftozone.domain.FeedbackService;
import com.appyaks.giftozone.web.SessionUtils;

@Controller
public class FeedbackController {

	@Inject
	private FeedbackService service;

	@RequestMapping(value = "/api/feedback", method = RequestMethod.POST)
	public void sendFeedback(@RequestBody final Feedback feedback) {
		service.sendFeedback(feedback, SessionUtils.getUserId());
	}

}