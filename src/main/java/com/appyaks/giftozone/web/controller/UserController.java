package com.appyaks.giftozone.web.controller;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;

import com.appyaks.giftozone.domain.FileData;
import com.appyaks.giftozone.domain.account.User;
import com.appyaks.giftozone.domain.account.UserBrief;
import com.appyaks.giftozone.domain.account.UserService;
import com.appyaks.giftozone.web.SessionUtils;

@Controller
public class UserController {

	@Inject
	private UserService userService;

	// TODO: Add the '/api' prefix to the URLs, otherwise they are not secured!

	@RequestMapping(value = "/user/current", method = RequestMethod.GET)
	public User getCurrentUser(Model uiModel, WebRequest request) {
		return userService.loadCurrentUser();
	}

	@RequestMapping(value = "/api/user/{id}", method = RequestMethod.GET)
	public UserBrief getUser(@PathVariable(value = "id") String userId) {
		return userService.getUserBrief(userId);
	}

	@RequestMapping(value = "/api/user/{id}", method = RequestMethod.POST)
	public User updateUser(@PathVariable(value = "id") String userId,
			@RequestBody User userAccount) {
		Assert.isTrue(userId.equals(SessionUtils.getUserId()));
		Assert.isTrue(userId.equals(userAccount.getUserId()));
		return userService.updateProfile(userAccount);
	}

	@RequestMapping(value = "/api/user/{userId}/contacts", method = RequestMethod.GET)
	public List<UserBrief> getContacts(
			@PathVariable(value = "userId") String userId) {
		Assert.isTrue(userId.equals(SessionUtils.getUserId()));
		return userService.getUserContactsBrief(userId);
	}

	@RequestMapping(value = "/api/user/{userId}/image", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public void getImage(
			@PathVariable(value = "userId") String userId, 
			HttpServletResponse response) {
		//Assert.isTrue(userId.equals(SessionUtils.getUserId()));
		FileData fileData = userService.getImage(userId);
		try {
			response.setContentType("image/jpeg");
			response.getOutputStream().write(fileData.getBytes());
		} catch (Exception e) {
			//TODO: Send back the default image:
			//response.getOutputStream().write(defaultAvatar);
		}
	}

	@RequestMapping(value = "/api/user/{userId}/image", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public void storeImage(
			@PathVariable(value = "userId") String userId, 
			@RequestPart("bytes") MultipartFile file) throws IOException {
		Assert.isTrue(userId.equals(SessionUtils.getUserId()));
		FileData fileData = new FileData();
		fileData.setBytes(file.getBytes());
		userService.storeImage(userId, fileData);
	}

	/** Unsecured URL */
	@RequestMapping(value = "/confirmRegistration", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public String confirmRegistration(
			@RequestParam(value = "userId") String userId, 
			@RequestParam(value = "confirmationId") String confirmationId) {
		try {
			userService.confirmRegistration(userId, confirmationId);
		} catch (Exception e) {
			return "signupConfirmationFailed";
		}
		return "signupConfirmed";
	}

}
