package com.appyaks.giftozone.web.controller;

import javax.inject.Inject;

import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.web.ConnectController;
import org.springframework.stereotype.Controller;

import com.appyaks.giftozone.web.SessionUtils;


@Controller
public class MyConnectController extends ConnectController {
    
    private static final String ACCOUNT_PAGE = "/app#/user/";

    @Inject
    public MyConnectController(ConnectionFactoryLocator connectionFactoryLocator, ConnectionRepository connectionRepository) {
        super(connectionFactoryLocator, connectionRepository);
    }

    protected String connectView(String providerId) {
        return "redirect:" + ACCOUNT_PAGE + SessionUtils.getUserId();
    }

    protected String connectedView(String providerId) {
        return "redirect:" + ACCOUNT_PAGE + SessionUtils.getUserId();
    }
    
}
