package com.appyaks.giftozone.web.controller;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.appyaks.giftozone.domain.Comment;
import com.appyaks.giftozone.domain.Occasion;
import com.appyaks.giftozone.domain.OccasionService;
import com.appyaks.giftozone.web.SessionUtils;

@Controller
public class OccasionController {

	@Inject
	private OccasionService occasionService;

	//TODO: We can remove headers="Accept=application/json" from all the methods, but make sure
	//  this header is set on the client when calling this method - $.ajax(..., dataType: "json", ...)

	//NOTE: If we want to be able to send requests in URL encoded format instead of JSON we need
	//  to change the @RequestBody annotation with @ModelAttribute.
	//
	//
	
	
	@RequestMapping(value = "/api/occasions", method = RequestMethod.GET)
	public List<Occasion> getOccasions(
			@RequestParam(value = "dateInMillis", required = false) Long dateInMillis,
			@RequestParam(value = "direction", required = false) String direction,
			@RequestParam(value = "limit", required = false) Integer limit) {
		/*
		 * TODO: Giftbag list is null, even though we always create at least one
		 * gift bag for an occasion.
		 */
		return occasionService.getUserOccasions(SessionUtils.getUserId(), dateInMillis, direction, limit);
	}

	@RequestMapping(value = "/api/occasions/{id}", method = RequestMethod.GET)
	public Occasion getOccasion(@PathVariable(value = "id") String occasionId) {
		return occasionService.getOccasion(occasionId, SessionUtils.getUserId());
	}

	@RequestMapping(value = "/api/occasions", method = RequestMethod.POST)
	public Occasion createOccasion(@Valid @RequestBody final Occasion occasion) {
		return occasionService.saveOccasion(occasion, SessionUtils.getUserId());
	}

	@RequestMapping(value = "/api/occasions/{id}", method = RequestMethod.POST)
	public Occasion updateOccasion(@PathVariable(value="id") String occasionId, @RequestBody final Occasion occasion) {
		occasion.setId(occasionId);
		return occasionService.saveOccasion(occasion, SessionUtils.getUserId());
	}

	@RequestMapping(value = "/api/occasions/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public void deleteOccasion(@PathVariable(value="id") String occasionId) {
		occasionService.deleteOccasion(occasionId, SessionUtils.getUserId());
	}

	
	@RequestMapping(value = "/api/occasions/{occasionId}/comments", method = RequestMethod.POST)
	public Comment createComment(@PathVariable(value="occasionId") String occasionId, @RequestBody final Comment comment) {
		return occasionService.saveOccasionComment(comment, occasionId, SessionUtils.getUserId());
	}

	@RequestMapping(value = "/api/occasions/{occasionId}/comments/{id}", method = RequestMethod.POST)
	public Comment updateComment(
			@PathVariable(value="occasionId") String occasionId,
			@PathVariable(value="commentId") String commentId,
			@RequestBody final Comment comment) {
		comment.setId(commentId);
		return occasionService.saveOccasionComment(comment, occasionId, SessionUtils.getUserId());
	}

	@RequestMapping(value = "/api/occasions/{occasionId}/comments/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public void deleteComment(
			@PathVariable(value="occasionId") String occasionId,
			@PathVariable(value="commentId") String commentId) {
		occasionService.deleteOccasionComment(commentId, occasionId, SessionUtils.getUserId());
	}

	
//	@RequestMapping(value = "/api/occasions/{occasionId}/participants/{participantId}", method = RequestMethod.POST)
//	@ResponseStatus(HttpStatus.OK)
//	public void inviteParticipant(
//			@PathVariable(value="occasionId") String occasionId,
//			@PathVariable(value="participantId") String participantId) {
//		occasionService.addParticipant(participantId, occasionId, SessionUtils.getUserId());
//	}
//

	@RequestMapping(value = "/api/occasions/{occasionId}/participants/{participantId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public void resignFromParticipants(
			@PathVariable(value="occasionId") String occasionId,
			@PathVariable(value="participantId") String participantId) {
		occasionService.deleteParticipant(participantId, occasionId, SessionUtils.getUserId());
	}

}
