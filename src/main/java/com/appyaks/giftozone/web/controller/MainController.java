package com.appyaks.giftozone.web.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.appyaks.giftozone.domain.InvitationService;
import com.appyaks.giftozone.web.SessionUtils;

@Controller
public class MainController {

	@Inject
	private InvitationService invitationService;

	@RequestMapping(value = "/")
	public String getIndex() {
		if (SessionUtils.isSignedIn()) {
			return "redirect:/app";
		}
		return "index";
	}

	@RequestMapping(value = "/app")
	public String getApp(HttpSession session) {
		
		// If we just accepted an invitation, finalize it:
		String invitationId = (String) session.getAttribute(InvitationController.INVITATION_KEY);
		if (invitationId != null) {
			invitationService.acceptOccasionInvitation(invitationId, SessionUtils.getUserId());
			session.removeAttribute(InvitationController.INVITATION_KEY);
		}
		
		return "app"; // refers to WEB-INF/jsp/app.jsp
	}
	
	@RequestMapping(value = "/occasions")
	public String getOccasions() {
		return "occasions"; // refers to WEB-INF/jsp/occasions.jsp
	}

	@RequestMapping(value = "/giftBag")
	public String getGiftBag() {
		return "giftBag";
	}

}
