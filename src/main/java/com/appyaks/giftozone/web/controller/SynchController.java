package com.appyaks.giftozone.web.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.appyaks.giftozone.domain.Synch;
import com.appyaks.giftozone.domain.SynchService;

@Controller
public class SynchController {
	
	@Inject
	private SynchService synchService;

	@RequestMapping(value = "/synch", method = RequestMethod.POST)
	public List<Synch> synch(@RequestBody List<Synch> synchs) {
		return synchService.confirmSynchs(synchs);
	}

}
