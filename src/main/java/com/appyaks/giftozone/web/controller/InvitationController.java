package com.appyaks.giftozone.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.appyaks.giftozone.domain.Invitation;
import com.appyaks.giftozone.domain.InvitationService;
import com.appyaks.giftozone.web.SessionUtils;

@Controller
public class InvitationController {

	public static final String INVITATION_KEY = "INVITATION_KEY";

	@Inject
	private InvitationService invitationService;

	@RequestMapping(value = "/api/invitations/occasions/{occasionId}/giftBags/{giftBagId}", method = RequestMethod.GET)
	public List<Invitation> getGiftBagInvitations(@PathVariable(value="occasionId") String occasionId, @PathVariable(value="giftBagId") String giftBagId) {
		//TODO: Pass in occasionId as well.
		return invitationService.getGiftBagInvitations(giftBagId);
	}

	@RequestMapping(value = "/api/invitations", method = RequestMethod.GET)
	public List<Invitation> getUserInvitations() {
		return invitationService.getInvitations(SessionUtils.getUserId());
	}
	
	@RequestMapping(value = "/api/invitations", method = RequestMethod.POST)
	public Invitation inviteUser(
			@RequestBody Invitation invitation) {
		return invitationService.inviteUserToOccasion(invitation, SessionUtils.getUserId());
	}

	/**
	 * This is an open URL. Here we let in both authenticated and not authenticated visitors, just to get
	 * the invitation key and store it in the session.
	 * */
	@RequestMapping(value = "/acceptinvitation/{invitationId}", method = RequestMethod.GET)
	public String acceptInvitationToOccasion(
			@PathVariable(value="invitationId") String invitationId,
			HttpSession session) {
		
		session.setAttribute(INVITATION_KEY, invitationId);
		
		// Now the user goes to login/registration page or, if the user is already authenticated, 
		// directly to the app where we will finish the invitation process. 
		
		return "redirect:/";
	}

	@RequestMapping(value = "/api/invitations/{invitationId}", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public void acceptInvitationToGiftBag(
			@PathVariable(value="invitationId") String invitationId) {
		invitationService.acceptGiftBagInvitation(invitationId, SessionUtils.getUserId());
	}

}
