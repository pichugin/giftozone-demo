package com.appyaks.giftozone.web.controller;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

import com.appyaks.giftozone.domain.account.FieldValidationException;
import com.appyaks.giftozone.domain.account.User;
import com.appyaks.giftozone.domain.account.UserService;

@Controller
public class AuthController {

	@Inject
	private UserService accountService;
	
	@Inject
	private PasswordEncoder passwordEncoder;
	

	@RequestMapping(value = "/loginfailed", method = RequestMethod.GET)
	public String getLoginFailure() {
		// This method handles the login failure after a non-Ajax request. For Ajax-based login see 
		// the SecurityLoginSuccessHandler / SecurityLoginFailureHandler.
		return "loginFailed";
	}
	
	@RequestMapping(value="/signup", method=RequestMethod.GET)
	public String getSignUp(@ModelAttribute("signupForm") SignupForm form) {
		return "signup";
	}
	
	@RequestMapping(value="/signup", method=RequestMethod.POST)  //, headers="Accept=application/json")
	public String signUp(@ModelAttribute("signupForm") @Valid SignupForm form, BindingResult formBinding, WebRequest request) {
	    if (formBinding.hasErrors()) {
	        return "signup";
	    }
	    User account = new User();
	    account.setEmail(form.getEmail());
	    account.setPassword(passwordEncoder.encode(form.getPassword()));
	    try {
	    	account = accountService.createUserAccount(account);
	    } catch (FieldValidationException e) {
	    	formBinding.rejectValue(e.getFieldName(), "accountCreation", e.getMessage());
	    	return "signup";
	    }
	    return "signupEmailSent";
	}

}
