package com.appyaks.giftozone.web.controller;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.appyaks.giftozone.domain.Comment;
import com.appyaks.giftozone.domain.CommentService;
import com.appyaks.giftozone.web.SessionUtils;

@Controller
public class CommentController {

	@Inject private CommentService commentService;
	
	@RequestMapping(value = "/api/comments", method = RequestMethod.POST)
	public Comment createComment(@RequestBody final Comment comment) {
		return commentService.addComment(comment, SessionUtils.getUserId());
	}

	@RequestMapping(value = "/api/comments/{commentId}", method = RequestMethod.POST)
	public Comment updateComment(@RequestBody final Comment comment) {
		return commentService.updateComment(comment, SessionUtils.getUserId());
	}

	@RequestMapping(value = "/api/comments/{commentId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public void deleteComment(@PathVariable(value="commentId") String commentId) {
		commentService.deleteComment(commentId, SessionUtils.getUserId());
	}

}
