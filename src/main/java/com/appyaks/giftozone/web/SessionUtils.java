package com.appyaks.giftozone.web;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.appyaks.giftozone.domain.account.User;

public class SessionUtils {
	
	public static String getUserId() {
		User user = getUser();
		return user == null ? null : user.getUserId();
	}

	public static String getUsername() {
		User user = getUser();
		return user == null ? null : user.getUsername();
	}

	public static User getUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
		     return (User) auth.getPrincipal();
		}
		return null;
	}

	public static boolean isSignedIn() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null && auth.getPrincipal() instanceof User) {
			return true;
		}
		return false;
	}

	public static void signIn(User userAccount) {
		SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(userAccount, null, null));	
	}
	
}
