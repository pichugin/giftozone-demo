package com.appyaks.giftozone.domain;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;


@Document
@SuppressWarnings("serial")
public abstract class BaseEntity implements Serializable {
    @Id
    private String id;
    
    private String owner;

	private Date created;
	
	private Date updated;
	
	@Field(value = "v")
	private int version;


    public String getId() {
        return id;
    }
    
    public void setId(String id) {
    	this.id = id;
    }
    
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
    @Override
    public int hashCode() {
        return (id == null) ? 0 : id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        BaseEntity other = (BaseEntity) obj;
        if (id == null) return other.id == null;
        return id.equals(other.id);
    }

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public <T extends BaseEntity> void copyBasicProperties(T entity) {
		this.created = entity.getCreated();
		this.owner = entity.getOwner();
		// Override this method and copy the entity's properties.
	}

}
