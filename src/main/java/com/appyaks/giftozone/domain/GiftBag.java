package com.appyaks.giftozone.domain;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
//@XmlRootElement
public class GiftBag extends BaseEntity {

	@Field(value = "occId")
	private String occasionId;
	
	@Field(value = "bdgt")
	private String budget;

	@Field(value = "prtcpnts")
	private List<String> participants;

	@Field(value = "cmnts")
	private List<Comment> comments;

	@Field(value = "prpsls")
	private List<GiftProposal> giftProposals;
	
	public GiftBag() {
		/* Framework needs the default constructor */
	}
	
	public GiftBag(String id) {
		setId(id);
	}
	
	public String getOccasionId() {
		return occasionId;
	}

	public void setOccasionId(String occasionId) {
		this.occasionId = occasionId;
	}

	public String getBudget() {
		return budget;
	}

	public void setBudget(String budget) {
		this.budget = budget;
	}

	public List<String> getParticipants() {
		return participants;
	}

	public void setParticipants(List<String> participants) {
		this.participants = participants;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<GiftProposal> getGiftProposals() {
		return giftProposals;
	}

	public void setGiftProposals(List<GiftProposal> giftProposals) {
		this.giftProposals = giftProposals;
	}

}
