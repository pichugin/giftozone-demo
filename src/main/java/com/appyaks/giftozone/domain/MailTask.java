package com.appyaks.giftozone.domain;

import java.util.Map;

public class MailTask implements Runnable {

	MailService mailService;
	Map<String, Object> model;

	public MailTask(MailService mailService, Map<String, Object> model) {
		this.mailService = mailService;
		this.model = model;
	}

	@Override
	public void run() {
		mailService.sendNow(model);
	}

}
