package com.appyaks.giftozone.domain;

public class VersionConflictException extends RuntimeException {

	private ConflictedEntities conflictedEntities;
	
	public VersionConflictException(BaseEntity oldObject, BaseEntity newObject) {
		this.conflictedEntities = new ConflictedEntities(oldObject, newObject);
	}

	public ConflictedEntities getConflictedEntities() {
		return conflictedEntities;
	}

	public class ConflictedEntities {
		private BaseEntity oldObject;
		private BaseEntity newObject;
		
		public ConflictedEntities(BaseEntity oldObject, BaseEntity newObject) {
			this.oldObject = oldObject;
			this.newObject = newObject;
		}

		public BaseEntity getOldObject() {
			return oldObject;
		}

		public BaseEntity getNewObject() {
			return newObject;
		}

	} 
	
}
