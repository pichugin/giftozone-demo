package com.appyaks.giftozone.domain;

public class FileData extends BaseEntity {

	private byte[] bytes;
	private String filename;
	private String mime;

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String name) {
		this.filename = name;
	}

	public String getMime() {
		return mime;
	}

	public void setMime(String mime) {
		this.mime = mime;
	}

}
