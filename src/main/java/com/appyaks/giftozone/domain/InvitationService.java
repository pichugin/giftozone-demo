package com.appyaks.giftozone.domain;

import java.util.List;

public interface InvitationService {

	Invitation inviteUserToOccasion(Invitation invitation, String occasionOwner);
	void acceptOccasionInvitation(String invitationId, String userId);
	void acceptGiftBagInvitation(String invitationId, String userId);
	List<Invitation> getGiftBagInvitations(String giftBagId);
	List<Invitation> getInvitations(String userId);

}
