package com.appyaks.giftozone.domain;

import java.util.List;

public interface GiftBagService {

	List<GiftBag> getUserGiftBags(String userId);
	List<GiftBag> getUserGiftBags(String userId, String occasionId);
	GiftBag saveGiftBag(GiftBag giftBag, String occasionId, String userId);
	GiftBag getGiftBag(String giftBagId, String occasionId, String userId);
	List<GiftBag> getGiftBags(String userId, String occasionId);
	void deleteGiftBag(String giftBagId, String userId);
	void deleteOccasionGiftBags(String occasionId);
	
	Comment addComment(Comment comment, String giftBagId, String userId);
	Comment updateComment(Comment comment, String giftBagId, String userId);
	void deleteComment(String commentId, String giftBagId, String userId);

	void addParticipant(String participantId, String giftBagId, String occasionId, String userId);
	void deleteParticipant(String participantId, String giftBagId, String occasionId, String userId);

	GiftProposal saveGiftProposal(GiftProposal giftProposal, String giftBagId, String occasionId, String userId);
	void deleteGiftProposal(String giftProposalId, String giftBagId, String occasionId, String userId);

	GiftProposal voteForProposal(String giftProposalId, String giftBagId, String occasionId, String userId);
	GiftProposal unvoteForProposal(String giftProposalId, String giftBagId, String occasionId, String userId);
	
}
