package com.appyaks.giftozone.domain;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.Date;

import javax.inject.Inject;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicUpdate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.appyaks.giftozone.domain.account.CounterService;

public class GiftBagRepositoryImpl implements GiftBagRepositoryCustom {

	@Inject
    private MongoTemplate mongoTemplate;
	
	@Inject
	private CounterService counterService;
    

	/*
	GiftBag : {
	_id : "...",
	occId : "...",
	bdgt : "",
	owner : "user1",
	created : Time(...),
	updated : Time(...),
	prtcpnts : [ "user1", "user2" ],
	cmnts : [
		{ id : "gbc1", text : "", owner : "user1" }
	],
	prpsls : [
		{ 
			id : "gp1",
			owner : "user1",
			nm : "",
			vtrs : [ "user1", "user2" ],
			cmnts : {
			  "gpc1" :  { text : "", owner : "user1" } 
			} 
		}
	]
}
*/
	
	//TODO: Don't forget to implement OccasionRepositoryImpl and add version support for occasions.

	@Override
	public GiftBag updateGiftBag(GiftBag giftBag) {
		Query query = new Query(Criteria
				.where("_id").is(giftBag.getId())
				.and("owner").is(giftBag.getOwner())
				.and("v").is(giftBag.getVersion()));
		
		Update update = new Update()
				.set("bdgt", giftBag.getBudget())
				.set("v", giftBag.getVersion() + 1)
				.set("updated", new Date());
		
		GiftBag updatedGiftBag = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), GiftBag.class);
		if (updatedGiftBag == null) {
			throw new VersionConflictException(giftBag, mongoTemplate.findOne(Query.query(
					where("_id").is(giftBag.getId())), GiftBag.class));
		}
		return updatedGiftBag;
	}
	
	@Override
	public void deleteGiftBag(String giftBagId, String owner) {
		Query query = new Query(where("_id").is(giftBagId).and("owner").is(owner));
		mongoTemplate.remove(query, GiftBag.class);
	}

	@Override
	public void addParticipant(String participant, String giftBagId, String giftBagOwner) {
		Query query = new Query(where("_id").is(giftBagId)
				.and("owner").is(giftBagOwner)
				.and("prtcpnts").ne(participant));
		
		Update update = new Update().push("prtcpnts", participant);
		
		mongoTemplate.findAndModify(query, update, /*new FindAndModifyOptions().returnNew(true),*/ GiftBag.class);
	}

	@Override
	public void deleteParticipant(String participant, String giftBagId, String giftBagOwner) {
		Query query = new Query(where("_id").is(giftBagId).and("owner").is(giftBagOwner));
		Update update = new Update().pull("prtcpnts", participant);
		mongoTemplate.findAndModify(query, update, GiftBag.class);
	}

	@Override
	public GiftBag addGiftProposal(GiftProposal proposal, String giftBagId, String giftBagParticipantToBeProposalOwner) {
		Query query = new Query(where("_id").is(giftBagId)
				.and("prtcpnts").is(giftBagParticipantToBeProposalOwner));

		Date now = new Date();
		proposal.setId("" + counterService.getNextProposalIdSequence()); //TODO: either do it on the DB level, or make sure we do "Optimistic Loop" when retrieving ids
		proposal.setOwner(giftBagParticipantToBeProposalOwner);
		proposal.setCreated(now);
		proposal.setUpdated(now);
		proposal.setVersion(1);

		Update update = new Update().push("prpsls", proposal);
		
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), GiftBag.class);
	}

	@Override
	public GiftBag updateGiftProposal(final GiftProposal proposal, String giftBagId, String proposalOwner) {
		Query query = new Query(where("_id").is(giftBagId)
				.and("prpsls").elemMatch(
						where("owner").is(proposalOwner)
						.and("_id").is(proposal.getId())
						.and("v").is(proposal.getVersion())));
		
		Update update = new Update()
				.set("prpsls.$.nm", proposal.getName())
				.set("prpsls.$.chsn", proposal.getChosen())
				.set("prpsls.$.cst", proposal.getCost())
				.set("prpsls.$.v", proposal.getVersion() + 1)
				.set("prpsls.$.updated", new Date());
		
		GiftBag updatedGiftBag = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), GiftBag.class);
		if (updatedGiftBag == null) {
			
			// TODO: refactoring
			
			query = new Query(where("_id").is(giftBagId)
					.and("prpsls").elemMatch(
							where("owner").is(proposalOwner)
							.and("_id").is(proposal.getId())));
			
			GiftBag gb = mongoTemplate.findOne(query, GiftBag.class);
			GiftProposal newProposal = gb != null ? BaseEntityUtil.findInListById(gb.getGiftProposals(), proposal.getId()) : null;
			throw new VersionConflictException(proposal, newProposal);
		}
		return updatedGiftBag;
	}

	@Override
	public void deleteGiftProposal(String proposalId, String giftBagId, String proposalOwner) {
		Query query = new Query(where("_id").is(giftBagId)
				.and("prpsls").elemMatch(
						where("owner").is(proposalOwner)
						.and("_id").is(proposalId)));

		BasicUpdate update = new BasicUpdate(String.format("{$pull:{'prpsls':{'_id':'%s'}}}", proposalId));
		mongoTemplate.updateFirst(query, update, GiftBag.class);
	}
	
	@Override
	public GiftBag addGiftProposalComment(Comment comment, String proposalId, String giftBagId, 
			String proposalParticipantToBeCommentOwner) {
		Query query = new Query(where("_id").is(giftBagId)
				.and("prpsls").elemMatch(
						where("prtcpnts").is(proposalParticipantToBeCommentOwner)
						.and("_id").is(proposalId)));

		Date now = new Date();
		comment.setUpdated(now);
		comment.setCreated(now);
		comment.setOwner(proposalParticipantToBeCommentOwner);
		
		Update update = new Update().set("prpsls.$.cmnts." + counterService.getNextProposalCommentIdSequence(), comment);
		
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), GiftBag.class);
	}

	@Override
	public GiftBag updateGiftProposalComment(Comment comment, String proposalId, String giftBagId, String commentOwner) {
		Query query = new Query(where("_id").is(giftBagId)
				.and("prpsls").elemMatch(
						where("prtcpnts").is(commentOwner)
						.and("_id").is(proposalId)
						.and("cmnts.v").is(comment.getVersion())));

		String fieldPrefix = "prpsls.$.cmnts." + comment.getId();
		Update update = new Update()
				.set(fieldPrefix + ".text", comment.getText())
				.set(fieldPrefix + ".v", comment.getVersion() + 1)
				.set(fieldPrefix + ".updated", new Date());
		
		GiftBag updatedGiftBag = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), GiftBag.class);
		if (updatedGiftBag == null) {
			
			// TODO: refactoring
			
			query = new Query(where("_id").is(giftBagId)
					.and("prpsls").elemMatch(
							where("prtcpnts").is(commentOwner)
							.and("_id").is(proposalId)));
			
			GiftBag gb = mongoTemplate.findOne(query, GiftBag.class);
			GiftProposal proposal = gb != null ? BaseEntityUtil.findInListById(gb.getGiftProposals(), proposalId) : null;
			throw new VersionConflictException(comment, proposal == null || proposal.getComments() == null 
					? null : proposal.getComments().get(comment.getId()));
		}
		return updatedGiftBag;
	}

	@Override
	public void deleteGiftProposalComment(String commentId, String proposalId, String giftBagId, String owner) {
		Query query = new Query(where("_id").is(giftBagId)
				.and("prpsls").elemMatch(
						where("prtcpnts").is(owner)
						.and("_id").is(proposalId)));

		Update update = new Update().unset("prpsls.$.cmnts." + commentId);
		
		mongoTemplate.findAndModify(query, update, GiftBag.class);
	}

	@Override
	public GiftBag addGiftProposalVote(String giftProposalId, String giftBagId, String giftBagParticipant) {
		Query query = new Query(where("_id").is(giftBagId)
				.and("prtcpnts").is(giftBagParticipant)
				.and("prpsls").elemMatch(
						where("_id").is(giftProposalId)  
						.and("vtrs").ne(giftBagParticipant)));

		Update update = new Update().push("prpsls.$.vtrs", giftBagParticipant);
		
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), GiftBag.class);
	}
	
	@Override
	public GiftBag deleteGiftProposalVote(String giftProposalId, String giftBagId, String giftBagParticipant) {
		Query query = new Query(where("_id").is(giftBagId)
				.and("prtcpnts").is(giftBagParticipant)
				.and("prpsls").elemMatch(
						where("_id").is(giftProposalId)  
						.and("vtrs").is(giftBagParticipant)));

		Update update = new Update().pull("prpsls.$.vtrs", giftBagParticipant);
		
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), GiftBag.class);
	}
	
}
