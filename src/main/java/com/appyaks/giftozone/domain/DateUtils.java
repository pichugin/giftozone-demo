package com.appyaks.giftozone.domain;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {

	
	public static Date appendCurrentTimeToDate(Date date) {
		Calendar calWithDate = Calendar.getInstance();
		calWithDate.setTime(date);
		
		Calendar calWithTime = Calendar.getInstance();
		calWithTime.setTime(new Date());
		calWithTime.set(Calendar.YEAR, calWithDate.get(Calendar.YEAR));
		calWithTime.set(Calendar.MONTH, calWithDate.get(Calendar.MONTH));
		calWithTime.set(Calendar.DAY_OF_MONTH, calWithDate.get(Calendar.DAY_OF_MONTH));
		return calWithTime.getTime();
	}
	
}
