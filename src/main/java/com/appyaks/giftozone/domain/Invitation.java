package com.appyaks.giftozone.domain;

public class Invitation extends BaseEntity {

	private String occasionId;
	private String giftBagId;
	private String invitee;
	private String email;
	private String text;
	private String link;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getOccasionId() {
		return occasionId;
	}

	public void setOccasionId(String occasionId) {
		this.occasionId = occasionId;
	}

	public String getGiftBagId() {
		return giftBagId;
	}

	public void setGiftBagId(String giftBagId) {
		this.giftBagId = giftBagId;
	}

	public String getInvitee() {
		return invitee;
	}

	public void setInvitee(String invitee) {
		this.invitee = invitee;
	}

}
