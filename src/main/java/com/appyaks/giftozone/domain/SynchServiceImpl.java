package com.appyaks.giftozone.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

public class SynchServiceImpl implements SynchService {
	
	public static final int SYNCH_AGE = 20 * 1000; //20sec
	
	@Inject
	private SynchRepository synchRepo;

	@Override
	public void saveSynch(Synch synch) {
		synchRepo.save(synch);
	}

	@Override
	public List<Synch> confirmSynchs(List<Synch> synchs) {
		List<String> refs = new ArrayList<String>();
		for (Synch synch : synchs) {
			refs.add(synch.getRef());
		}
		return synchRepo.findNewSynchs(new Date(System.currentTimeMillis() - SYNCH_AGE), refs);
	}

}
