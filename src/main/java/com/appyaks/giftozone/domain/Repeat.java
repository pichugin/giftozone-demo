package com.appyaks.giftozone.domain;

import java.util.Calendar;
import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Field;

public class Repeat {

	public static final int YEARLY = Calendar.YEAR;    // 1
	public static final int MONTHLY = Calendar.MONTH;  // 2
	
	@Field(value = "freq")
	private int frequency;
	
	private Date date;

	
	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public Date getNextDate() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(frequency, 1);
		return cal.getTime();
	}
	
}
