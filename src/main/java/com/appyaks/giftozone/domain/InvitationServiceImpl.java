package com.appyaks.giftozone.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.util.Assert;

import com.appyaks.giftozone.domain.account.UserService;

public class InvitationServiceImpl implements InvitationService {

	@Inject
	private InvitationRepository invitationRepository;
	@Inject
	private OccasionRepository occasionRepository;
	@Inject
	private GiftBagRepository giftBagRepository;
	@Inject
	private MailService mailService;
	@Inject
	private UserService userService;
	
	
	@Override
	public Invitation inviteUserToOccasion(Invitation invitation, String occasionOwner) {
		Occasion occ = occasionRepository.findByIdAndOwnerOrderByIdAsc(invitation.getOccasionId(), occasionOwner);
		Assert.notNull(occ, "Occasion doesn't exist or you don't have permissions to invite participants to this occasion.");
		
		//TODO: Move this to InvitationRepositoryCustom:
		Date now = new Date();
		invitation.setOwner(occasionOwner);
		invitation.setCreated(now);
		invitation.setUpdated(now);
		
		invitationRepository.save(invitation);
		
		invitation.setLink("This link is deprecated - an email has been sent to the invitee."); //TODO: Remove this link from Invitation
		if (invitation.getEmail() != null) {
			mailService.sendOccasionInvitation(userService.findByUserId(occasionOwner), invitation.getId(), invitation.getEmail());
		}
		
		return invitation;
	}

	@Override
	public void acceptOccasionInvitation(String invitationId, String userId) {
		Invitation invitation = invitationRepository.findOne(invitationId);
		Assert.notNull(invitation, "Looks like the invitation has expired. Please request another invitation.");
		Occasion occ = occasionRepository.findOne(invitation.getOccasionId());
		if (occ != null) {
			List<String> participants = occ.getParticipants();
			if (participants == null) {
				participants = new ArrayList<String>();
				occ.setParticipants(participants);
			}
			if (!participants.contains(userId)) {
				participants.add(userId);
				occasionRepository.save(occ);
			}
		}
		//TODO: In the future don't delete the invitation right away - what if the user 
		//      looses the session in the middle of the process and wants to re-click on the link...
		if (invitation.getGiftBagId() == null) {
			invitationRepository.delete(invitation);
		} else if (invitation.getInvitee() == null) {
			// Now this invitation belongs to the user accepting it,
			// and we will show it as a pending invitation for the gift bag:
			invitation.setInvitee(userId);
			invitationRepository.save(invitation);
		}

		//@Deprecated See User.contacts
		//TODO: Wrap it into one transaction somehow:
		//userAccountRepository.addContact(invitation.getOwner(), userId);
		//userAccountRepository.addContact(userId, invitation.getOwner());
	}
	
	public void acceptGiftBagInvitation(String invitationId, String userId) {
		Invitation invitation = invitationRepository.findOne(invitationId);
		
		Assert.notNull(invitation, "Looks like the invitation has expired. Please request another invitation.");
		Assert.isTrue(invitation.getInvitee().equals(userId), "This invitation belongs to another user.");
		
		giftBagRepository.addParticipant(userId, invitation.getGiftBagId(), invitation.getOwner());
		invitationRepository.delete(invitation);
	}
	
	public void rejectGiftBagInvitation(String invitationId, String userId) {
		//TODO: Move this to InvitationRepositoryCustom:
		Invitation invitation = invitationRepository.findOne(invitationId);
		if (invitation != null) {
			invitation.setGiftBagId(null);
			invitationRepository.save(invitation);
		}
	}

	@Override
	public List<Invitation> getGiftBagInvitations(String giftBagId) {
		return invitationRepository.findByGiftBagIdOrderByCreatedAsc(giftBagId);
	}

	@Override
	public List<Invitation> getInvitations(String userId) {
		return invitationRepository.findByInviteeOrderByCreatedDesc(userId);
	}

}
