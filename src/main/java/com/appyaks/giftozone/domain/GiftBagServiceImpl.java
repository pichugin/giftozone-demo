package com.appyaks.giftozone.domain;

import static com.appyaks.giftozone.domain.Synch.SYNCH_CONTAINER_TYPE_COMMENT;
import static com.appyaks.giftozone.domain.Synch.SYNCH_CONTAINER_TYPE_GIFTBAG;
import static com.appyaks.giftozone.domain.Synch.SYNCH_CONTAINER_TYPE_OCCASION;
import static com.appyaks.giftozone.domain.Synch.SYNCH_OPERATION_CREATE;
import static com.appyaks.giftozone.domain.Synch.SYNCH_OPERATION_DELETE;
import static com.appyaks.giftozone.domain.Synch.SYNCH_OPERATION_UPDATE;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.util.Assert;



public class GiftBagServiceImpl implements GiftBagService {
	
	@Inject
	private GiftBagRepository giftBagRepo;
	@Inject
	private OccasionRepository occasionRepo;
	@Inject
	private SynchRepository synchRepo;
	@Inject
	private CommentService commentService;
	
	@Override
	public List<GiftBag> getUserGiftBags(String userId) {
		return null; //TODO
	}

	@Override
	public List<GiftBag> getUserGiftBags(String userId, String occasionId) {
		return null; //TODO
	}
	
	@Override
	public GiftBag saveGiftBag(GiftBag giftBag, String occasionId, final String userId) {
		giftBag.setOccasionId(occasionId);
		giftBag.setOwner(userId);

		if (giftBag.getId() == null) {
			giftBag.setCreated(new Date());
			giftBag.setUpdated(giftBag.getCreated());
			giftBag.setParticipants(new ArrayList<String>() {{ add(userId); }}); //Did you see that! :)))
			giftBag = giftBagRepo.save(giftBag);
			synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_OCCASION, occasionId, SYNCH_OPERATION_UPDATE));
		} else {
			giftBag = giftBagRepo.updateGiftBag(giftBag);
			synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_GIFTBAG, giftBag.getId(), SYNCH_OPERATION_UPDATE));
		}
		return giftBag;
	}

	/**
	 * Returns all the gift bags that belong to the occasion, event those ones where the user is 
	 * not a participant - we want the user to see all gift bags.
	 * */
	@Override
	public List<GiftBag> getGiftBags(String userId, String occasionId) {
		assertUserParticipateInOccasion(userId, occasionId);
		return giftBagRepo.findByOccasionIdOrderByIdAsc(occasionId);
	}

	@Override
	public GiftBag getGiftBag(String giftBagId, String occasionId, String userId) {
		assertUserParticipateInOccasion(userId, occasionId);
		GiftBag giftBag = giftBagRepo.findOne(giftBagId); //findByIdAndParticipantsOrderByIdAsc(giftBagId, userId);
		if (giftBag != null) {
			giftBag.setComments(commentService.getCommentTree(
					Comment.COMMENT_TYPE_GIFTBAG, 
					giftBagId, null, Comment.DEPTH_UNLIMITED));
		}
		return giftBag;
	}

	private void assertUserParticipateInOccasion(String userId, String occasionId) {
		Assert.notNull(occasionRepo.findByIdAndParticipantsOrderByIdAsc(occasionId, userId),
				"The user is not a participant in the occasion");
	}
	
	@Override
	public void deleteGiftBag(String giftBagId, String userId) {
		giftBagRepo.deleteGiftBag(giftBagId, userId);
		synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_GIFTBAG, giftBagId, SYNCH_OPERATION_DELETE));
		//TODO: Update Occasion if it has a reference to the GiftBag
	}

	@Override
	public void deleteOccasionGiftBags(String occasionId) {
		giftBagRepo.delete(giftBagRepo.findByOccasionIdOrderByIdAsc(occasionId));
		synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_OCCASION, occasionId, SYNCH_OPERATION_UPDATE));
	}


	@Override
	public Comment addComment(Comment comment, String giftBagId, String userId) {
		comment.setId(null);
		comment.setType(Comment.COMMENT_TYPE_GIFTBAG);
		comment.setRefId(giftBagId);
		comment = commentService.addComment(comment, userId);
		synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_COMMENT, comment.getId(), giftBagId, SYNCH_OPERATION_CREATE));
		return comment;
	}

	@Override
	public Comment updateComment(Comment comment, String giftBagId, String userId) {
		Comment updatedComment = commentService.updateComment(comment, userId);
		if (updatedComment == null) {
			comment.setId(null);
			return addComment(comment, giftBagId, userId);
		} else {
			synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_COMMENT, comment.getId(), SYNCH_OPERATION_UPDATE));
		}
		return updatedComment;
	}

	@Override
	public void deleteComment(String commentId, String giftBagId, String userId) {
		commentService.deleteComment(commentId, userId);
		synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_COMMENT, commentId, giftBagId, SYNCH_OPERATION_DELETE));
	}

	@Override
	public void addParticipant(String participantId, String giftBagId, String occasionId, String userId) {
		giftBagRepo.addParticipant(participantId, giftBagId, userId);
		synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_GIFTBAG, giftBagId, SYNCH_OPERATION_UPDATE));
	}

	@Override
	public void deleteParticipant(String participantId, String giftBagId, String occasionId, String userId) {
		giftBagRepo.deleteParticipant(participantId, giftBagId, userId);
		synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_GIFTBAG, giftBagId, SYNCH_OPERATION_UPDATE));
	}

	@Override
	public GiftProposal saveGiftProposal(GiftProposal giftProposal, String giftBagId, String occasionId, String userId) {
		if (giftProposal.getId() == null) {
			giftBagRepo.addGiftProposal(giftProposal, giftBagId, userId);
		} else {
			GiftBag updatedGiftBag = giftBagRepo.updateGiftProposal(giftProposal, giftBagId, userId);
			for (GiftProposal proposal : updatedGiftBag.getGiftProposals()) {
				if (proposal.getId().equals(giftProposal.getId())) {
					giftProposal = proposal;
					break;
				}
			}
		}
		synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_GIFTBAG, giftBagId, SYNCH_OPERATION_UPDATE));
		return giftProposal;
	}

	@Override
	public void deleteGiftProposal(String giftProposalId, String giftBagId, String occasionId, String userId) {
		giftBagRepo.deleteGiftProposal(giftProposalId, giftBagId, userId);
		synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_GIFTBAG, giftBagId, SYNCH_OPERATION_UPDATE));
	}

	@Override
	public GiftProposal voteForProposal(String giftProposalId, String giftBagId, String occasionId, String userId) {
		GiftBag giftBag = giftBagRepo.addGiftProposalVote(giftProposalId, giftBagId, userId);
		GiftProposal proposal = giftBag.getGiftProposals().get(giftBag.getGiftProposals().indexOf(new GiftProposal(giftProposalId)));
		synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_GIFTBAG, giftBagId, SYNCH_OPERATION_UPDATE));
		return proposal;
	}

	@Override
	public GiftProposal unvoteForProposal(String giftProposalId, String giftBagId, String occasionId, String userId) {
		GiftBag giftBag = giftBagRepo.deleteGiftProposalVote(giftProposalId, giftBagId, userId);
		GiftProposal proposal = giftBag.getGiftProposals().get(giftBag.getGiftProposals().indexOf(new GiftProposal(giftProposalId)));
		synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_GIFTBAG, giftBagId, SYNCH_OPERATION_UPDATE));
		return proposal;
	}
	
}
