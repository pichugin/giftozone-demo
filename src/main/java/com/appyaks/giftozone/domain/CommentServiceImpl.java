package com.appyaks.giftozone.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.util.Assert;

public class CommentServiceImpl implements CommentService {

	@Inject
	private CommentRepository commentRepository;
	
	//TODO: Pass userId to check permissions 
	@Override
	public List<Comment> getCommentTree(int commentType, String refId, String parentCommentId, int maxDepth) {

		List<Comment> comments = commentRepository.findByTypeAndRefIdOrderByCreatedAsc(commentType, refId);
		
		return buildChildrenRecursively(comments, null);
	}
	
	//TODO: Optimize the algorithm
	private List<Comment> buildChildrenRecursively(List<Comment> comments, String parentId) {
		List<Comment> children = new ArrayList<Comment>();
		for (Comment c : comments) {
			if (parentId == null && c.getParentId() == null || parentId != null && parentId.equals(c.getParentId())) {
				c.setComments(buildChildrenRecursively(comments, c.getId()));
				children.add(c);
			}
		}
		return children;
	}

	@Override
	public Comment addComment(Comment comment, String userId) {
		Date now = new Date();
		comment.setCreated(now);
		comment.setUpdated(now);
		comment.setOwner(userId);
		return commentRepository.save(comment);
	}

	@Override
	public Comment updateComment(Comment comment, String userId) {
		Comment oldComment = getComment(comment.getId());
		if (oldComment == null) {
			return null;
		}
		Assert.isTrue(oldComment.getOwner().equals(userId), "Update failed: comment does not belong to the user!");
		oldComment.setUpdated(new Date());
		oldComment.setText(comment.getText());
		return commentRepository.save(oldComment);
	}

	@Override
	public void deleteComment(String id, String userId) {
		Comment comment = commentRepository.findOne(id);
		if (comment != null) {
			Assert.isTrue(comment.getOwner().equals(userId), "Delete failed: comment does not belong to the user!");
			commentRepository.delete(id);
		}
	}

	@Override
	public Comment getComment(String id) {
		return commentRepository.findOne(id);
	}

}
