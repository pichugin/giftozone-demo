package com.appyaks.giftozone.domain;

public interface FeedbackService {

	void sendFeedback(Feedback feedback, String userId);

}
