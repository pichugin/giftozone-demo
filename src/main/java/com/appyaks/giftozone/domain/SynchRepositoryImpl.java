package com.appyaks.giftozone.domain;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

public class SynchRepositoryImpl implements SynchRepositoryCustom {

	@Inject
    private MongoTemplate mongoTemplate;

	@Override
	public List<Synch> findNewSynchs(Date lastUpdated, List<String> refs) {
		Query query = new Query(where("_id").gt(new ObjectId(lastUpdated)).and("ref").in(refs));
		return mongoTemplate.find(query, Synch.class);
	}

}
