package com.appyaks.giftozone.domain;

import java.util.Date;

import javax.inject.Inject;

public class FeedbackServiceImpl implements FeedbackService {

	private String feedbackEmail;

	@Inject
	private MailService mailService;

	public FeedbackServiceImpl(String feedbackEmail) {
		this.feedbackEmail = feedbackEmail;
	}

	@Override
	public void sendFeedback(Feedback feedback, String userId) {
		Date now = new Date();
		feedback.setOwner(userId);
		feedback.setCreated(now);
		feedback.setUpdated(now);

		mailService.sendFeedback(feedback, feedbackEmail);
	}

}
