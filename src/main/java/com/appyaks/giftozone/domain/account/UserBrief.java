package com.appyaks.giftozone.domain.account;

import org.springframework.data.mongodb.core.mapping.Document;

import com.appyaks.giftozone.domain.BaseEntity;

@Document(collection = "UserAccount") //TODO: change to User
public class UserBrief extends BaseEntity {
	
	private String userId;
	private String email;
	private String displayName;
	private String imageUrl;
	private String webSite;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getWebSite() {
		return webSite;
	}

	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}

}
