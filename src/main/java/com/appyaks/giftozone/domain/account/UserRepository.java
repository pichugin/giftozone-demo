package com.appyaks.giftozone.domain.account;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;


public interface UserRepository extends MongoRepository<User, String>, UserRepositoryCustom {
    
    User findByUserId(String userId);
    User findByEmail(String email);
    User findByUserIdAndConfId(String userId, String confirmationId);

    Page<User> findAllOrderByUserId(Pageable pageable);

	@Query(value="{ 'userId' : { $in : ?0 } }", fields = "{ 'userId' : 1, 'email' : 1, 'displayName' : 1, 'imageUrl' : 1 }")
	List<UserBrief> findUserBriefs(List<String> userIds);

	@Query(value="{ 'userId' : ?0 }", fields = 
			"{ "
			+ "'userId' : 1, "
			+ "'email' : 1, "
			+ "'displayName' : 1, "
			+ "'imageUrl' : 1, "
			+ "'webSite' : 1 "
			+ "}")
	User findUserLight(String userId);

}
