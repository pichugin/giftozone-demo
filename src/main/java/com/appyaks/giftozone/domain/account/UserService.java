package com.appyaks.giftozone.domain.account;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.UserIdSource;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.security.SocialUserDetailsService;

import com.appyaks.giftozone.domain.FileData;


public interface UserService extends SocialUserDetailsService, UserDetailsService, UserIdSource {
    
	User loadCurrentUser();
	User updateProfile(User userAccount);
	
	List<UserBrief> getUserContactsBrief(String userId);
	UserBrief getUserBrief(String userId);
	User findByUserIdLight(String userId);
	
    User findByUserId(String userId);
    User findByEmail(String email);

    List<User> getAllUsers();
    Page<User> getAllUsers(Pageable pageable);

    List<UserSocialConnection> getConnectionsByUserId(String userId);
    
    User createUserAccount(ConnectionData data);
    User createUserAccount(User userAccount);

    User updateImageUrl(String userId, String imageUrl);

    void lockAccount(String userId, boolean locked);

//    void trustAccount(String userId, boolean trusted);

    /**
     * Override SocialUserDetailsService.loadUserByUserId(String userId) to return User.
     */
    User loadUserByUserId(String userId) throws UsernameNotFoundException;

	FileData getImage(String userId);
	void storeImage(String userId, FileData fileData);
	void confirmRegistration(String userId, String confirmationId);
	
}
