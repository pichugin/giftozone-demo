package com.appyaks.giftozone.domain.account;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.connect.ConnectionData;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.appyaks.giftozone.domain.FileData;
import com.appyaks.giftozone.domain.MailService;


public class UserServiceImpl implements UserService {
	
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final CounterService counterService;
	private final UserRepository accountRepository;
	private final UserSocialConnectionRepository userSocialConnectionRepository;
	private final MailService mailService;

	
	@Inject
	public UserServiceImpl(UserRepository accountRepository, UserSocialConnectionRepository 
			userSocialConnectionRepository, CounterService counterService, MailService mailService) {
		this.accountRepository = accountRepository;
		this.userSocialConnectionRepository = userSocialConnectionRepository;
		this.counterService = counterService;
		this.mailService = mailService;
	}

	@Override
	public User findByUserId(String userId) {
		return accountRepository.findByUserId(userId);
	}

	@Override
	public User findByEmail(String email) {
		return accountRepository.findByEmail(email);
	}

	@Override
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<User> getAllUsers() {
		return accountRepository.findAll();
	}

	@Override
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Page<User> getAllUsers(Pageable pageable) {
		return accountRepository.findAll(pageable);
	}

	@Override
	public List<UserSocialConnection> getConnectionsByUserId(String userId){
		return this.userSocialConnectionRepository.findByUserId(userId);
	}

	@Override
	public User updateProfile(User userAccount) throws UsernameNotFoundException {
		User account = loadUserByUserId(userAccount.getUserId());
		account.updateProfile(userAccount.getDisplayName(), userAccount.getEmail());
		account.setImageUrl(userAccount.getImageUrl());
		accountRepository.save(account);
		return account;
	}

	@Override
	@PreAuthorize("hasRole('ROLE_USER')")
	public User updateImageUrl(String userId, String imageUrl) throws UsernameNotFoundException {
		User account = loadUserByUserId(userId);
		account.setImageUrl(imageUrl);
		accountRepository.save(account);
		return account;
	}

	@Override
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public void lockAccount(String userId, boolean locked) {
		User account = loadUserByUserId(userId);
		account.setAccountLocked(locked);
		this.accountRepository.save(account);
	}

//	@Override
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
//	public void trustAccount(String userId, boolean trusted) {
//		User account = loadUserByUserId(userId);
//			account.setTrustedAccount(trusted);
//			this.accountRepository.save(account);
//	}

	@Override
	public User loadCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null && auth.getPrincipal() instanceof User) {
			User account = (User) auth.getPrincipal();
			account = findByUserIdLight(account.getUserId()); //findByUserId(account.getUserId());
			account.setConnections(getConnectionsByUserId(account.getUserId()));
			
			//check duplicate connection error //TODO: Do we need this???
//			if (request.getParameter("social.addConnection.duplicate") != null){
//				System.out.println("Duplicate Connection!!!???");
//			}
			return account;
		}
		return null;
	}

	@Override
	public List<UserBrief> getUserContactsBrief(String userId) {
		List<String> contactsIds = accountRepository.findContacts(userId);
		return accountRepository.findUserBriefs(contactsIds);
	}
	
	@Override
	public UserBrief getUserBrief(String userId) {
		List<UserBrief> accounts = accountRepository.findUserBriefs(Arrays.asList(new String[] {userId}));
		if (accounts.size() == 0) {
			return null;
		}
		return accounts.get(0);
	}

	@Override
	public User findByUserIdLight(String userId) {
		User userLight = accountRepository.findUserLight(userId);
		if (userLight == null) {
			return null;
		}
		userLight.setConnections(getConnectionsByUserId(userId));
		return userLight;
	}

	/**
	 * Create a user when signing up using a social network:
	 * */
	@Override
	public User createUserAccount(ConnectionData data) {
		User user = new User();
		user.setDisplayName(data.getDisplayName());
		user.setImageUrl(data.getImageUrl());
		return saveUser(user);
	}

	/**
	 * Create a user when signing up through regular registration mechanism:
	 * */
	@Transactional
	@Override
	public User createUserAccount(User user) {
		user.setConfId(generateAccountConfirmationKey());
		saveUser(user);
		mailService.sendAccountConfirmation(user);
		return user;
	}

	private User saveUser(User user) {
		long userIdSequence = this.counterService.getNextUserIdSequence();
		user.setUserId(String.valueOf(userIdSequence));
		try {
			accountRepository.save(user);
		} catch (DuplicateKeyException e) {
			//TODO: Currently we assume the duplicate field is email since we don't have other constraints,
			// but in the future we might have other unique fields. Unfortunately, I don't see a convenient 
			// way to extract the information about the field that caused the exception rather than just parsing the message.
			throw new FieldValidationException("email", "User with this email already exists. "
					+ "If this is your email and you don't remember the password you can "
					+ "make a password reset request. Otherwise, use another email for registration.", e);
		}
		return user;
	}

	private String generateAccountConfirmationKey() {
		return UUID.randomUUID().toString();
	}

	@Override
	public User loadUserByUserId(String userId) throws UsernameNotFoundException {
		User account = accountRepository.findByUserId(userId);
		if (account == null) {
			throw new UsernameNotFoundException("Cannot find user by userId " + userId);
		}
		return account;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return loadUserByUserId(username);
	}

	@Override
	public String getUserId() {
		return UserUtils.getLoginUserId();
	}

	@Override
	public FileData getImage(String userId) {
		return accountRepository.readImage(userId);
	}

	@Override
	public void storeImage(String userId, FileData fileData) {
		accountRepository.storeImage(userId, fileData);		
	}
	
	@Override
	public void confirmRegistration(String userId, String confirmationId) {
		User user = accountRepository.findByUserId(userId);
		if (StringUtils.isEmpty(user.getConfId()) || user.getConfId().equals(confirmationId)) {
			user.setConfId(null);
			accountRepository.save(user);
		} else {
			throw new RuntimeException("Wrong confirmation key!");
		}
	}

}
