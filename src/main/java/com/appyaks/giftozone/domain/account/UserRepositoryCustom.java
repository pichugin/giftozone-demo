package com.appyaks.giftozone.domain.account;

import java.util.List;

import com.appyaks.giftozone.domain.FileData;


public interface UserRepositoryCustom {

	@Deprecated //See User.contacts
    User addContact(String contactId, String userId);
	@Deprecated //See User.contacts
    User removeContact(String contactId, String userId);

    List<String> findContacts(String userId);
    
	void storeImage(String userId, FileData fileData);
	FileData readImage(String userId);

}
