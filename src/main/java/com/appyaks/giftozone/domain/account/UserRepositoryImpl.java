package com.appyaks.giftozone.domain.account;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.gridfs.GridFsOperations;

import com.appyaks.giftozone.domain.FileData;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;

public class UserRepositoryImpl implements UserRepositoryCustom {

	@Inject
    private MongoTemplate mongoTemplate;
	
	@Inject
    private GridFsOperations gridOperations;


	@Deprecated //See User.contacts
	@Override
	public User addContact(String contactId, String userId) {
		Query query = new Query(where("_id").is(userId).and("cntcts").ne(contactId));
		Update update = new Update().push("cntcts", contactId);
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), User.class);
	}

	@Deprecated //See User.contacts
	@Override
	public User removeContact(String contactId, String userId) {
		Query query = new Query(where("_id").is(userId).and("cntcts").is(contactId));
		Update update = new Update().pull("cntcts", contactId);
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), User.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<String> findContacts(String userId) {
		
		//TODO: I guess the only way to implement pagination in this case is to use something like the following:
		//db.occasion.aggregate(
		//	{$project : {owner:1, participants:1}}, 
		//	{$unwind: "$participants"}, 
		//	{$sort:{participants:1}}, 
		//	{$match: {owner:"user9", participants:{$gte:"user9"}}})
		
		DBObject query = new BasicDBObject();
		query.put("owner", userId);
		return mongoTemplate.getCollection("occasion").distinct("participants", query);
	}

	@Override
	public void storeImage(String userId, FileData fileData) {
		//TODO: Use something other than userId as the filename to support multiple files per user:
		gridOperations.delete(new Query(where("filename").is(userId)));
		gridOperations.store(new ByteArrayInputStream(fileData.getBytes()), userId, fileData.getMime());
	}

	@Override
	public FileData readImage(String userId) {

		List<GridFSDBFile> result = gridOperations.find(new Query(where("filename").is(userId)));
	 
		FileData fileData = new FileData();
		for (GridFSDBFile file : result) {
			try {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				file.writeTo(baos);
				fileData.setBytes(baos.toByteArray());
				break;
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		return fileData;
	}
	
}
