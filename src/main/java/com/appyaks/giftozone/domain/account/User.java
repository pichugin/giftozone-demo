package com.appyaks.giftozone.domain.account;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.util.StringUtils;

import com.appyaks.giftozone.domain.BaseEntity;

@SuppressWarnings("serial")
@Document(collection = "UserAccount")  // TODO: Change to User
@XmlRootElement
public class User extends BaseEntity implements SocialUserDetails { // TODO: Maybe extend UserBrief
	
	@Indexed
	private String userId;
	
	private String email;
	
	//TODO: Uncomment the annotations when we finish development:
	//@Field (value = "emlCnfrmd")
	private Boolean emailConfirmed;
	
	//@Field (value = "name")
	private String displayName;

	//@Field (value = "imgUrl")
	private String imageUrl;
	
//	private String webSite;
	
	@Field (value = "lck")
	private boolean accountLocked;
	
	@Field (value = "lckRsn")
	private String lockReason;
	
//	private boolean trustedAccount;
	private String password; // For local accounts;
	private String confId;

	// I am turning off the contacts. Instead, at least for now, let's treat all
	// the users who share occasions
	// as contacts. This would give them a reasonably wide circle to search in.
	// @Deprecated
	// private List<String> contacts;

	@Transient
	private List<UserSocialConnection> connections;


	public User() {
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public boolean isAccountLocked() {
		return accountLocked;
	}

	public void setAccountLocked(boolean accountLocked) {
		this.accountLocked = accountLocked;
	}

	public List<UserSocialConnection> getConnections() {
		return connections;
	}

	public void setConnections(List<UserSocialConnection> connections) {
		this.connections = connections;
	}

	List<UserRoleType> roles = Arrays.asList(UserRoleType.ROLE_USER);
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return roles;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return !accountLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return StringUtils.isEmpty(confId);
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return getUserId();
	}

	public boolean isHasImageUrl() {
		return StringUtils.hasLength(getImageUrl());
	}

	// used for account social connection
	private UserSocialConnection getConnection(String providerId) {
		if (this.connections != null) {
			for (UserSocialConnection connection : this.connections) {
				if (connection.getProviderId().equals(providerId)) {
					return connection;
				}
			}
		}
		return null;
	}

	public UserSocialConnection getGoogleConnection() {
		return getConnection("google");
	}

	public boolean isHasGoogleConnection() {
		return getGoogleConnection() != null;
	}

	public UserSocialConnection getTwitterConnection() {
		return getConnection("twitter");
	}

	public boolean isHasTwitterConnection() {
		return getTwitterConnection() != null;
	}

	public UserSocialConnection getFacebookConnection() {
		return getConnection("facebook");
	}

	public boolean isHasFacebookConnection() {
		return getFacebookConnection() != null;
	}

	public void updateProfile(String displayName, String email) {
		setDisplayName(displayName);
		setEmail(email);
	}

	@Override
	public String toString() {
		String str = String.format(
				"User{userId:'%s'; displayName:'%s';roles:[", getUserId(),
				getDisplayName());
//		for (UserRoleType role : getRoles()) {
//			str += role.toString() + ",";
//		}
		return str + "]}";
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfId() {
		return confId;
	}

	public void setConfId(String confId) {
		this.confId = confId;
	}

	public Boolean getEmailConfirmed() {
		return emailConfirmed;
	}

	public void setEmailConfirmed(Boolean emailConfirmed) {
		this.emailConfirmed = emailConfirmed;
	}

	// @Deprecated //See User.contacts
	// public List<String> getContacts() {
	// return contacts;
	// }
	//
	// @Deprecated //See User.contacts
	// public void setContacts(List<String> contacts) {
	// this.contacts = contacts;
	// }

}
