package com.appyaks.giftozone.domain.account;

public class FieldValidationException extends RuntimeException {

	private String fieldName;
	
	public FieldValidationException(String fieldName, String message, Throwable cause) {
		super(message, cause);
		this.fieldName = fieldName;
	}

	public FieldValidationException(String fieldName, String message) {
		super(message);
		this.fieldName = fieldName;
	}
	
	public String getFieldName() {
		return fieldName;
	}

}
