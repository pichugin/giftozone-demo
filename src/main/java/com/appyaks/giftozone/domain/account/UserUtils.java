package com.appyaks.giftozone.domain.account;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


public class UserUtils {

    private UserUtils() {}

    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public static User getLoginUserAccount() {
        if (getAuthentication() != null && getAuthentication().getPrincipal() instanceof User) {
            return (User)getAuthentication().getPrincipal();
        }
        return null;
    }

    public static String getLoginUserId() {
        User account = getLoginUserAccount();
        return (account == null) ? null : account.getUserId();
    }

}
