package com.appyaks.giftozone.domain;

import java.util.List;

public interface SynchService {

	void saveSynch(Synch synch);
	
	List<Synch> confirmSynchs(List<Synch> synchs);
	
}
