package com.appyaks.giftozone.domain;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OccasionRepository extends MongoRepository<Occasion, String>, OccasionRepositoryCustom {

	List<Occasion> findByOwnerOrderByCreatedDesc(String userId);
	List<Occasion> findByParticipantsOrderByDateAsc(String userId);
	List<Occasion> findByParticipantsAndDateLessThanOrderByDateDesc(String userId, Date date, Pageable pageable);
	List<Occasion> findByParticipantsAndDateGreaterThanOrderByDateAsc(String userId, Date date, Pageable pageable);
	Occasion findByIdAndOwnerOrderByIdAsc(String occasionId, String ownerId);
	Occasion findByIdAndParticipantsOrderByIdAsc(String occasionId, String participantId);
	
}
