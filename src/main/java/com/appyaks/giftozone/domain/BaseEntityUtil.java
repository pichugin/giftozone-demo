package com.appyaks.giftozone.domain;

import java.util.ArrayList;
import java.util.List;

public class BaseEntityUtil {

	public static <T extends BaseEntity> List<T> putToList(List<T> entities, T entity, String userId) {
		if (entities == null) {
			entities = new ArrayList<T>();
		}
		if (entity.getId() == null) {
			entities.add(entity);
		} else {
			int i = entities.indexOf(entity);
			if (i >= 0) {
				T c = entities.get(i);
				if (!c.getOwner().equals(userId)) {
					throw new RuntimeException("You cannot modify this object");
				}
				c.copyBasicProperties(entity);
				entity = c;
			} else {
				entity.setId(null);
				entities.add(entity);
			}
		}
		return entities;
	}
	
	public static <T extends BaseEntity> boolean removeFromList(List<T> comments, T comment, String userId) {
		if (comments == null) {
			return false;
		}
		int i = comments.indexOf(comment);
		if (i >= 0) {
			T c = comments.get(i);
			if (!c.getOwner().equals(userId)) {
				throw new RuntimeException("You cannot delete this object");
			}
			comments.remove(i);
			return true;
		}
		return false;
	}
	
	public static <T extends BaseEntity> T findInListById(List<T> list, String id) {
		if (list == null || id == null) {
			return null;
		}
		for (T entity : list) {
			if (id.equals(entity.getId())) {
				return entity;
			}
		}
		return null;
	}

}
