package com.appyaks.giftozone.domain;

import java.util.Map;

import com.appyaks.giftozone.domain.account.User;

public interface MailService {

	void sendAccountConfirmation(User user);
	void sendOccasionReminder(User user, Occasion occasion);
	void sendOccasionInvitation(User inviter, String invitationId, String email);
	void sendFeedback(Feedback feedback, String email);
	void sendNow(Map<String, Object> model);
	
}
