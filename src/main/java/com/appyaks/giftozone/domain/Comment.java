package com.appyaks.giftozone.domain;

import java.util.List;

import org.springframework.data.annotation.Transient;

public class Comment extends BaseEntity {
	
	public static final int COMMENT_TYPE_GIFTBAG  = 1;
	public static final int COMMENT_TYPE_OCCASION = 2;
	public static final int COMMENT_TYPE_PROPOSAL = 3;
	
	public static final int DEPTH_UNLIMITED = 0;

	private Integer type;
	private String refId;
    private String parentId;
    private Integer depth;
	private String text;
	
	@Transient
	private List<Comment> comments;

	public Comment() {
	}

	public Comment(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	@Override
	public <T extends BaseEntity> void copyBasicProperties(T entity) {
		super.copyBasicProperties(entity);
		Comment comment = (Comment) entity;
		setText(comment.getText());
		setParentId(comment.getParentId());
	}

	public Integer getDepth() {
		return depth;
	}

	public void setDepth(Integer depth) {
		this.depth = depth;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

}
