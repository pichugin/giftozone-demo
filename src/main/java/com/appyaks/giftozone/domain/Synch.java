package com.appyaks.giftozone.domain;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
@SuppressWarnings("serial")
public class Synch implements Serializable {
	
	public static final String SYNCH_OPERATION_CREATE = "C";
	public static final String SYNCH_OPERATION_UPDATE = "U";
	public static final String SYNCH_OPERATION_DELETE = "D";
	public static final String SYNCH_CONTAINER_TYPE_OCCASION = "OC";
	public static final String SYNCH_CONTAINER_TYPE_GIFTBAG  = "GB";
	public static final String SYNCH_CONTAINER_TYPE_COMMENT  = "CO";

	@Id
	private String id;

	@Field(value = "type")
	private String containerType;     //TODO: We haven't used it so far. Consider removing this field.
	
	private String ref;        // Id of the object
	private String refParent;  // Id of the parent object if any, can be used if the object was created
	
	@Field(value = "op")
	private String operation;

	
	public Synch() {}

	public Synch(String containerType, String ref, String operation) {
		this.containerType = containerType;
		this.ref = ref;
		this.operation = operation;
	}

	public Synch(String containerType, String ref, String refParent, String operation) {
		this(containerType, ref, operation);
		this.refParent = refParent;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContainerType() {
		return containerType;
	}

	public void setContainerType(String containerType) {
		this.containerType = containerType;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getRefParent() {
		return refParent;
	}

	public void setRefParent(String refParent) {
		this.refParent = refParent;
	}
	
}
