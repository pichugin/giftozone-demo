package com.appyaks.giftozone.domain;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface CommentRepository extends MongoRepository<Comment, String> {

	List<Comment> findByTypeAndRefIdOrderByCreatedAsc(Integer type, String refId);
//	List<Comment> findByParentId(String parentId);
	
}
