package com.appyaks.giftozone.domain;

import java.util.Date;
import java.util.List;

public interface SynchRepositoryCustom {

	List<Synch> findNewSynchs(Date lastUpdated, List<String> refs);

}
