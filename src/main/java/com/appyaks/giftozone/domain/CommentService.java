package com.appyaks.giftozone.domain;

import java.util.List;

public interface CommentService {
	
	Comment getComment(String id);
	List<Comment> getCommentTree(int commentType, String commentedObjectId, String parentCommentId, int maxDepth);
	Comment addComment(Comment comment, String userId);
	Comment updateComment(Comment comment, String userId);
	void deleteComment(String id, String userId);
	
}
