package com.appyaks.giftozone.domain;

public interface GiftBagRepositoryCustom {

	GiftBag updateGiftBag(GiftBag giftBag);
	void deleteGiftBag(String giftBagId, String owner);

	void addParticipant(String participant, String giftBagId, String giftBagOwner);
	void deleteParticipant(String participant, String giftBagId, String giftBagOwner);
	
	GiftBag addGiftProposal(GiftProposal proposal, String giftBagId, String giftBagParticipantToBeProposalOwner);
	GiftBag updateGiftProposal(GiftProposal proposal, String giftBagId, String proposalOwner);
	void deleteGiftProposal(String proposalId, String giftBagId, String proposalOwner);
	
	GiftBag addGiftProposalComment(Comment comment, String proposalId, String giftBagId, String participant);
	GiftBag updateGiftProposalComment(Comment comment, String proposalId, String giftBagId, String commentOwner);
	void deleteGiftProposalComment(String commentId, String proposalId, String giftBagId, String owner);

	GiftBag addGiftProposalVote(String giftProposalId, String giftBagId, String giftBagParticipant);
	GiftBag deleteGiftProposalVote(String giftProposalId, String giftBagId, String giftBagParticipant);
	
}
