package com.appyaks.giftozone.domain;

import static com.appyaks.giftozone.domain.Synch.SYNCH_CONTAINER_TYPE_OCCASION;
import static com.appyaks.giftozone.domain.Synch.SYNCH_OPERATION_DELETE;
import static com.appyaks.giftozone.domain.Synch.SYNCH_OPERATION_UPDATE;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.util.Assert;

import com.appyaks.giftozone.domain.account.CounterService;

public class OccasionServiceImpl implements OccasionService {

	private static final int MAX_ALLOWED_PAGE_SIZE = 50;
	private static final String DIRECTION_UP = "UP";
			
	//TODO: Get rid of these id prefixes...
    private static final String OCCASION_COMMENT_ID_PREFIX = "occ";

	@Inject
    private CounterService counterService;
	@Inject
	private OccasionRepository occasionRepo;
	@Inject
	private GiftBagService giftBagService;
	@Inject
	private SynchRepository synchRepo;
	
	
	/**
	 * Returns paginated occasions where the user is a participant.
	 * */
	@Override
	public List<Occasion> getUserOccasions(String userId, Long dateInMillis, String direction, Integer limit) {
		List<Occasion> occasions = occasionRepo.findByParticipantsOrderByDateAsc(userId);
		
		//TODO: The following paginated version works, but we have to make the client smart enough to handle
		//      pagination. One of the ideas is to load the entire list of occasions with basic info, like date and id,
		//      and then feed this list to the calendar. And then the paginated lists of full objects will be loaded on demand
		//      when triggered either by scroll or calendar.

		//		Date date = dateInMillis == null ? new Date() : new Date(dateInMillis);
		//		Pageable pageLimit = new PageRequest(0, limit == null || limit > MAX_ALLOWED_PAGE_SIZE ? MAX_ALLOWED_PAGE_SIZE : limit);
		//		List<Occasion> occasions = DIRECTION_UP.equalsIgnoreCase(direction)
		//				? occasionRepo.findByParticipantsAndDateGreaterThanOrderByDateAsc(userId, date, pageLimit)
		//				: occasionRepo.findByParticipantsAndDateLessThanOrderByDateDesc(userId, date, pageLimit);
		
		for (Occasion occ : occasions) {
			occ.setGiftBags(giftBagService.getGiftBags(userId, occ.getId()));
		}
		return occasions;
	}

	@Override
	public Occasion saveOccasion(Occasion occasion, String userId) {
		if (occasion.getId() != null) {
			Occasion persistedOccasion = occasionRepo.findByIdAndOwnerOrderByIdAsc(occasion.getId(), userId);
			Assert.notNull(persistedOccasion, "Occasion not found");
			//TODO: Move the field copying into Occasion: 
			persistedOccasion.setDescription(occasion.getDescription());
			persistedOccasion.setRecipient(occasion.getRecipient());
			persistedOccasion.setDate(occasion.getDate());
			persistedOccasion.setRepeat(occasion.getRepeat());
			persistedOccasion.setUpdated(new Date());
			occasion = persistedOccasion;
			synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_OCCASION, occasion.getId(), SYNCH_OPERATION_UPDATE));
		} else {
			occasion.setCreated(new Date());
			occasion.setOwner(userId);
		}
		
		occasion.setDate(DateUtils.appendCurrentTimeToDate(occasion.getDate()));
		
		occasion.setUpdated(new Date());
		List<String> participants = occasion.getParticipants();
		if (participants == null) {
			participants = new ArrayList<String>();
			occasion.setParticipants(participants);
		}
		if (!participants.contains(userId)) {
			participants.add(userId);
		}
		occasion = occasionRepo.save(occasion);
		//Add first gift bag for new occasion
		if (occasion.getGiftBags() == null) {
			GiftBag newBag = new GiftBag();
			newBag.setOccasionId(occasion.getId());
			giftBagService.saveGiftBag(newBag, occasion.getId(), userId);
		}
		occasion.setGiftBags(giftBagService.getGiftBags(userId, occasion.getId()));
		return occasion;
	}

	@Override
	public Occasion getOccasion(String occasionId, String userId) {
		Occasion occasion = occasionRepo.findByIdAndParticipantsOrderByIdAsc(occasionId, userId);
		occasion.setGiftBags(giftBagService.getGiftBags(userId, occasionId));
		return occasion;
	}

	private Occasion getOccasionAsserted(String occasionId, String userId) {
		Occasion occasion = getOccasion(occasionId, userId);
		Assert.notNull(occasion, "Occasion not found");
		return occasion;
	}
	
	@Override
	public void deleteOccasion(String occasionId, String userId) {
		Occasion occasion = occasionRepo.findByIdAndOwnerOrderByIdAsc(occasionId, userId);
		if (occasion != null) {
			occasionRepo.delete(occasion);
			giftBagService.deleteOccasionGiftBags(occasionId);
		}
		synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_OCCASION, occasion.getId(), SYNCH_OPERATION_DELETE));
	}

	@Override
	public Comment saveOccasionComment(Comment comment, String occasionId, String userId) {
		Occasion occasion = getOccasionAsserted(occasionId, userId);
		comment.setOwner(userId);
		occasion.setComments(BaseEntityUtil.putToList(occasion.getComments(), comment, userId));
		if (comment.getId() == null) {
			comment.setId(OCCASION_COMMENT_ID_PREFIX + counterService.getNextOccasionCommentIdSequence());
		}
		occasionRepo.save(occasion);
		synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_OCCASION, occasion.getId(), SYNCH_OPERATION_UPDATE));
		return comment;
	}

	@Override
	public void deleteOccasionComment(String commentId, String occasionId, String userId) {
		Occasion occasion = getOccasionAsserted(occasionId, userId);
		Comment comment = new Comment();
		comment.setId(commentId);
		if (BaseEntityUtil.removeFromList(occasion.getComments(), comment, userId)) {
			occasionRepo.save(occasion);
			synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_OCCASION, occasion.getId(), SYNCH_OPERATION_UPDATE));
		}
	}

	@Override
	public void addParticipant(String participantId, String occasionId, String userId) {
		Occasion occasion = occasionRepo.findByIdAndOwnerOrderByIdAsc(occasionId, userId);
		Assert.notNull(occasion, "Occasion not found / not enough permissions to modify the occasion");
		List<String> participants = occasion.getParticipants();
		if (participants == null) {
			participants = new ArrayList<String>();
		}
		if (!participants.contains(participantId)) {
			participants.add(participantId);
			occasionRepo.save(occasion);
			synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_OCCASION, occasion.getId(), SYNCH_OPERATION_UPDATE));
			//TODO: send an email to the invited person
		}
	}

	@Override
	public void deleteParticipant(String participantId, String occasionId, String userId) {
		Occasion occasion = participantId.equals(userId) 
				? occasionRepo.findByIdAndParticipantsOrderByIdAsc(occasionId, participantId)
				: occasionRepo.findByIdAndOwnerOrderByIdAsc(occasionId, userId);
		Assert.notNull(occasion, "Occasion not found / not enough permissions to modify the occasion");
		List<String> participants = occasion.getParticipants();
		if (participants != null && participants.remove(participantId)) {
			occasionRepo.save(occasion);
			synchRepo.save(new Synch(SYNCH_CONTAINER_TYPE_OCCASION, occasion.getId(), SYNCH_OPERATION_UPDATE));
		}		
	}
	
	@Override
	public Occasion recurOccasion(String occasionId, String userId) {
		Occasion oldOccasion = occasionRepo.findByIdAndOwnerOrderByIdAsc(occasionId, userId);
		Assert.notNull(oldOccasion, "Cannot recur occasion. Occasion not found!");
		Date now = new Date();
		Occasion occasion = new Occasion();
		occasion.setOwner(userId);
		occasion.setCreated(now);
		occasion.setUpdated(now);
		occasion.setDescription(oldOccasion.getDescription());
		occasion.setRecipient(oldOccasion.getRecipient());
		occasion.setParticipants(oldOccasion.getParticipants());
		Repeat repeat = oldOccasion.getRepeat();
		occasion.setDate(repeat.getDate());
		repeat.setDate(repeat.getNextDate());
		occasion.setRepeat(repeat);
		return occasionRepo.save(occasion);
		
		//TODO: Notify participants.
	}
	
}
