package com.appyaks.giftozone.domain;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Field;

public class GiftProposal extends BaseEntity {

	@Field(value = "nm")
	private String name;

	@Field(value = "chsn")
	private Boolean chosen;
	
	@Field(value = "cst")
	private Integer cost;
	
	@Field(value = "vtrs")
	private List<String> voters;

	@Field(value = "cmnts")
	private Map<String, Comment> comments;

	
	public GiftProposal() {
		/* Framework needs the default constructor */
	}
	
	public GiftProposal(String id) {
		setId(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getVoters() {
		return voters;
	}

	public void setVoters(List<String> voters) {
		this.voters = voters;
	}

	public Map<String, Comment> getComments() {
		return comments;
	}

	public void setComments(Map<String, Comment> comments) {
		this.comments = comments;
	}

	public Boolean getChosen() {
		return chosen;
	}

	public void setChosen(Boolean chosen) {
		this.chosen = chosen;
	}

	public Integer getCost() {
		return cost;
	}

	public void setCost(Integer cost) {
		this.cost = cost;
	}

	@Override
	public <T extends BaseEntity> void copyBasicProperties(T entity) {
		super.copyBasicProperties(entity);
		GiftProposal giftProposal = (GiftProposal) entity;
		this.name = giftProposal.getName();
	}

}
