package com.appyaks.giftozone.domain;

public class Feedback extends BaseEntity {

	private String userId;
	private String occasionId;
	private String giftBagId;
	private String severity;
	private String message;

	public Feedback() {
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOccasionId() {
		return occasionId;
	}

	public void setOccasionId(String occasionId) {
		this.occasionId = occasionId;
	}

	public String getGiftBagId() {
		return giftBagId;
	}

	public void setGiftBagId(String giftBagId) {
		this.giftBagId = giftBagId;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Feedback [userId=" + userId + ", occasionId=" + occasionId
				+ ", giftBagId=" + giftBagId + ", severity=" + severity
				+ ", message=" + message + "]";
	}

}