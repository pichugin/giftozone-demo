package com.appyaks.giftozone.domain;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.jboss.logging.Logger;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.util.Assert;

import com.appyaks.giftozone.domain.account.User;

public class MailServiceImpl implements MailService {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Inject
	private JavaMailSender mailSender;
    @Inject
    private Environment environment;
    @Inject
    private VelocityEngine velocityEngine;
    @Inject
    private TaskExecutor mailTaskExecutor;

    
	@Override
	public void sendAccountConfirmation(final User user) {
		Assert.notNull(user.getEmail(), "Email is required for registration.");

		//TODO: Move to i18n properties:
		String subject = "Giftozone: Account confirmation";
		String confirmationUrl = environment.getProperty("account.confirmation.url")
				.replace("{userId}", user.getUserId())
				.replace("{confirmationId}", user.getConfId());
		
		logger.info("Confirmation URL: " + confirmationUrl); //TODO: remove

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("user", user);
		model.put("confirmationUrl", confirmationUrl);
		
		scheduleEmail(user.getEmail(), subject, model, "accountConfirmation.vm");
	}
	
	@Override
	public void sendOccasionReminder(User user, Occasion occasion) {
		Assert.notNull(user.getEmail(), "Failed to send a reminder: The user doesn't have an email in the profile.");

		//TODO: Move to i18n properties:
		String subject = "Giftozone: Reminder";
		String reminderUrl = environment.getProperty("occasion.reminder.url").replace("{id}", occasion.getId());

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("user", user);
		model.put("reminderUrl", reminderUrl);
		
		scheduleEmail(user.getEmail(), subject, model, "occasionReminder.vm");
	}

	@Override
	public void sendOccasionInvitation(User inviter, String invitationId, String email) {
		Assert.notNull(email, "Failed to send an invitation: email is not provided.");

		//TODO: Move to i18n properties:
		String subject = "Giftozone: Invitation to participate";
		String invitationUrl = environment.getProperty("occasion.invitation.url").replace("{id}", invitationId);

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("invitingUser", inviter);
		model.put("invitationUrl", invitationUrl);
		
		scheduleEmail(email, subject, model, "occasionInvitation.vm");
	}

	@Override
	public void sendFeedback(Feedback feedback, String email) {
		Assert.notNull(email, "Failed to send feedback: email is not provided.");

		String subject = "Giftozone: Feedback form (" + feedback.getSeverity() + ")";

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("userId", feedback.getUserId());
		model.put("occasionId", feedback.getOccasionId());
		model.put("giftBagId", feedback.getGiftBagId());
		model.put("severity", feedback.getSeverity());
		model.put("message", feedback.getMessage());

		scheduleEmail(email, subject, model, "feedback.vm");
	}

	@Override
	public void sendNow(Map<String, Object> model) {
		String email = (String) model.get("_email");
		String subject = (String) model.get("_subject");
		String velocityTemplate = (String) model.get("_velocityTemplate");
		mailSender.send(buildEmailPreparator(email, subject, model, velocityTemplate));
	}

	private void scheduleEmail(String email, String subject, Map<String, Object> model, String template) {
		model.put("_email", email);
		model.put("_subject", subject);
		model.put("_velocityTemplate", template);
		mailTaskExecutor.execute(new MailTask(this, model));
	}

	private MimeMessagePreparator buildEmailPreparator(
			final String email,
			final String subject,
			final Map<String, Object> model,
			final String template) {
		
		return new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
				message.setTo(email);
				message.setFrom(environment.getProperty("mail.username"));
				message.setSubject(subject);
				String text = VelocityEngineUtils.mergeTemplateIntoString(
						velocityEngine, "" + template, "UTF-8", model);
				message.setText(text, true);
			}
		};
	}

}
