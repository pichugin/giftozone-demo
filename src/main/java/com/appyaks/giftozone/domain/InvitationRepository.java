package com.appyaks.giftozone.domain;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface InvitationRepository extends MongoRepository<Invitation, String>/* , InvitationRepositoryCustom */{

	List<Invitation> findByGiftBagIdOrderByCreatedAsc(String giftBagId);

	List<Invitation> findByInviteeOrderByCreatedDesc(String invitee);

}
