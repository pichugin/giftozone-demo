package com.appyaks.giftozone.domain;

import java.util.Date;
import java.util.List;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
public class Occasion extends BaseEntity {

	@NotEmpty
	@Length(max = 1000)
	private String description;
	
	@NotEmpty
	@Length(max = 500)
	private String recipient;
	
	private Date date;
	
	private List<String> participants;
	private List<Comment> comments;
	private Repeat repeat;
	
	@Transient
	private List<GiftBag> giftBags;

	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Repeat getRepeat() {
		return repeat;
	}

	public void setRepeat(Repeat repeat) {
		this.repeat = repeat;
	}

	public List<String> getParticipants() {
		return participants;
	}

	public void setParticipants(List<String> participants) {
		this.participants = participants;
	}

	public List<GiftBag> getGiftBags() {
		return giftBags;
	}

	public void setGiftBags(List<GiftBag> giftBags) {
		this.giftBags = giftBags;
	}
	
	@Transient
	public Long getDateInMillis() {
		return date == null ? null : date.getTime();
	}

}
