package com.appyaks.giftozone.domain;

import java.util.List;

public interface OccasionService {

	List<Occasion> getUserOccasions(String userId, Long dateInMillis, String direction, Integer limit);
	Occasion saveOccasion(Occasion occasion, String userId);
	Occasion getOccasion(String occasionId, String userId);
	void deleteOccasion(String occasionId, String userId);
	
	Comment saveOccasionComment(Comment comment, String occasionId, String userId);
	void deleteOccasionComment(String commentId, String occasionId, String userId);
	
	void addParticipant(String participantId, String occasionId, String userId);
	void deleteParticipant(String participantId, String occasionId, String userId);
	
	Occasion recurOccasion(String occasionId, String userId);
	
}
