package com.appyaks.giftozone.domain;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface GiftBagRepository extends MongoRepository<GiftBag, String>, GiftBagRepositoryCustom {

    List<GiftBag> findByOccasionIdAndParticipantsOrderByIdAsc(String occasionId, String participantId /* userId */);
    List<GiftBag> findByOccasionIdOrderByIdAsc(String occasionId);
    GiftBag findByIdAndParticipantsOrderByIdAsc(String giftBagId, String participantId);
    GiftBag findByIdAndOwnerOrderByIdAsc(String giftBagId, String ownerId);

}
