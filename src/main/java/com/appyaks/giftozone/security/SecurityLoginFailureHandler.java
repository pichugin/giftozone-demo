package com.appyaks.giftozone.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

public class SecurityLoginFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
					throws IOException, ServletException {
		
		System.out.println("SecurityLoginFailureHandler");
		
		//TODO: Ideally we should JSONize the Auth object here, but for now let's just return a hardcoded string:
		
		if ("XMLHttpRequest".equals(request.getHeader("X-Requested-With"))) {
			response.getWriter().print("{\"success\":false, \"message\": \"Username/Password are invalid\"}");
			response.getWriter().flush();
		} else {
			super.onAuthenticationFailure(request, response, exception);
		}
	}
	
}
