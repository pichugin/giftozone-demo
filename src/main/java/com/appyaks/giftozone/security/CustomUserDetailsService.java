package com.appyaks.giftozone.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.appyaks.giftozone.domain.account.User;
import com.appyaks.giftozone.domain.account.UserService;

public class CustomUserDetailsService implements UserDetailsService {
	 
	@Inject
	private UserService accountService;
 
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	String email = username; 
        User user = accountService.findByEmail(email);
        if (user == null) {
        	throw new UsernameNotFoundException("User does not exist.");
        }
        return user;
        
        
//    	try {
//			boolean enabled = true; //TODO: Boolean.TRUE.equals(user.getActive()), etc.
//			boolean accountNonExpired = true;
//			boolean credentialsNonExpired = true;
//			boolean accountNonLocked = true;
//
//			return new org.springframework.security.core.userdetails.User(
//					account.getEmail(), //user.getUsername(), 
//					account.getPassword().toLowerCase(),
//					enabled,
//					accountNonExpired,
//					credentialsNonExpired,
//					accountNonLocked,
//					getAuthorities(account));
//
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
    }
    
    public Collection<? extends GrantedAuthority> getAuthorities(User user) {
		
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
//		if (Boolean.TRUE.equals(user.getType().getAdmin())) {
//			authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
//		}
//		if (Boolean.TRUE.equals(user.getType().getDev())) {
//			authorities.add(new SimpleGrantedAuthority("ROLE_DEV"));
//		}
//		if (Boolean.TRUE.equals(user.getType().getGen())) {
//			authorities.add(new SimpleGrantedAuthority("ROLE_GEN"));
//		}
//		if (authorities.size() > 0) {
			authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
//		}

		return authorities;
	}

}
